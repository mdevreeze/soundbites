import { shallow } from "enzyme";
import "jest";
import * as React from "react";
import { IBite } from "soundbites-shared";
import { BiteCard, CardButton } from "../src/components/shared/bite-card";

const bite: IBite = {
  _id: "",
   audioFilename: "somesound.mp3",
   createDate: new Date(),
   imageFilename: "someimage.jpg",
   origin: "youtube",
   originReference: "someref",
   playCount: 10,
   title: "Test bite",
   originVideoRange: [10, 15],
   tags: [],
   userId: "someuserid"
};
test("Bite card snapshot", () => {
  const wrapper = shallow(<BiteCard bite={bite} buttons={[CardButton.Download, CardButton.Delete, CardButton.Edit]}/>);
  expect(wrapper).toMatchSnapshot();
});
