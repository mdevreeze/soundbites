function sum(a: number, b: number) {
  return a + b;
}

test("test sum", () => {
  expect(sum(1, 2)).toBe(3);
});
