import { Card, Col, Icon, Popconfirm, Tag } from "antd";
import { Howl } from "howler";
import * as React from "react";
import Scrollbars from "react-custom-scrollbars";
import { IBite } from "soundbites-shared";
// import { HttpStatus, IMbState } from "../../../redux";

const { Meta } = Card;

export enum CardButton {
  Edit,
  Delete,
  Download
}

export interface IBiteCardProps {
  bite?: IBite;
  buttons?: CardButton[];
  className?: string;
  style?: React.CSSProperties;
  onButtonClick?: (button: CardButton, byte: IBite) => any;
}

export interface IBiteCardState {
  isLoaded: boolean;
  playing: boolean;
  howlId: number;
}

export class BiteCard extends React.PureComponent<IBiteCardProps, IBiteCardState> {
  private howl: Howl;
  public constructor(props: IBiteCardProps, state: IBiteCardState) {
    super(props, state);
    this.state = {
      playing: false,
      howlId: undefined,
      isLoaded: false
    };
  }

  public render() {

    if (!this.props.bite) {
      return <div><p>Loading...</p></div>;
    }
    return this.renderBite();
  }

  public renderBite() {
    let playStatus = "";
    let action = this.onPlayClickHandler;
    if (this.state.playing) {
      playStatus = "playing";
      action = this.onStopClickHandler;
    }
    const cover = (
      <div
        style={{
          height: "12em",
          // width: "calc(18em - 2px)",
          textAlign: "center",
          backgroundImage:
            `url(http://localhost:3000/media/covers/${(this.props.bite.imageFilename || "no-image.jpg")})`,
          backgroundSize: "cover"
        }}
      >
        <div className={"control " + playStatus} onClick={action} />
      </div>
    );
    return (
      <Col
        className={`gutter-row ${this.props.className}`}
        xs={12}
        sm={8}
        md={8}
        lg={6}
        xl={6}
        style={this.props.style}
      >
        <Card
          key={this.props.bite._id}
          cover={cover}
          className=""
          // style={{ width: "18em", float: "left", margin: "1em 1em 4em 1em" }}
          actions={this.getButtons()}
        >
          <Meta
            title={this.props.bite.title}
            // style={{ height: "4em" }}
            description={this.getDescription()}
          />
        </Card >
      </Col>
    );
  }

  public componentWillUnmount() {
    if (this.howl) {
      this.howl.unload();
    }
  }

  public getButtons(): JSX.Element[] {
    const buttons = [];
    if (!this.props.buttons) {
      return [];
    }
    if (this.props.buttons.indexOf(CardButton.Delete) > -1) {
      buttons.push(<Icon key="download" type="download" onClick={this.handleDownloadClick} />);
    }
    if (this.props.buttons.indexOf(CardButton.Edit) > -1) {
      buttons.push(<Icon key="edit" type="edit" onClick={this.handleEditClick} />);
    }
    if (this.props.buttons.indexOf(CardButton.Delete) > -1) {
      buttons.push(
        <Popconfirm
          title="Are you sure delete this bite?"
          onConfirm={this.handleDeleteClick}
          okText="Yes"
          cancelText="No"
        >
          <Icon key="delete" type="delete" />
        </Popconfirm>
      );
    }
    return buttons;
  }

  private getDescription() {
    return (
      <Scrollbars style={{ width: "100%", height: 35, whiteSpace: "nowrap" }}>
          {this.props.bite.tags.map((t, i) => <Tag closable={false} key={i}>{t}</Tag>)}
      </Scrollbars>
    );
  }

  private handleDownloadClick = () => this.props.onButtonClick(CardButton.Download, this.props.bite);
  private handleEditClick = () => this.props.onButtonClick(CardButton.Edit, this.props.bite);
  private handleDeleteClick = () => this.props.onButtonClick(CardButton.Delete, this.props.bite);

  private onPlayClickHandler = () => {
    if (!this.state.isLoaded) {
      const howl = new Howl({
        format: "mp3",
        src: `${process.env.API_HOST}/media/audio/${this.props.bite.audioFilename}`,
        onend: () => this.setState({ playing: false })
      });
      howl.once("load", () => {
        howl.play();
        const id = howl.play();
        this.howl = howl;
        this.setState({ playing: howl.playing(), howlId: id, isLoaded: true });
      });
    } else {
      this.howl.play();
      const id = this.howl.play();
      this.setState({ ...this.state, playing: this.howl.playing(), howlId: id });
    }
    fetch(`${process.env.API_HOST}/bites/${this.props.bite._id}/playCount`, {method: "PUT"});
  }

  private onStopClickHandler = () => {
    this.howl.pause();
    this.howl.stop();
    this.setState({ playing: this.howl.playing() });
  }
}

export default BiteCard;
