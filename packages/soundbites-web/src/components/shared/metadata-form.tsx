import { Button, Form, Icon, Input, Upload } from "antd";
import { FormComponentProps } from "antd/lib/form";
import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { IBite } from "soundbites-shared";
// import { bindActionCreators } from "redux";
import { TagInput } from "./tag-input";
const Dragger = Upload.Dragger;
const FormItem = Form.Item;
const titleRules = {
  rules: [
    {
      message: "Enter a title",
      required: true,
    }
  ],
};

export interface IMetaDataFormProps extends FormComponentProps, RouteComponentProps<any> {
  onSubmitClick?: (youtubeCode: string) => any;
  bite?: IBite;
  disabled: boolean;
  saveBite?: (newBite: IBite) => any;
  goBack: () => any;
}

export class MetaDataForm extends React.Component<IMetaDataFormProps, { imageFilename: string, imageObj: string }> {
  public constructor(props: IMetaDataFormProps, state: { imageUrl: string }) {
    super(props, state);
    this.state = {
      imageObj: undefined,
      imageFilename: undefined
    };
  }

  public render() {
    const formItemLayout = {
      labelCol: {
        sm: { span: 6 },
        xs: { span: 24 }
      },
      wrapperCol: {
        sm: { span: 10 },
        xs: { span: 24 }
      },
    };
    return (
      <Form>
        <FormItem {...formItemLayout} label="Title:" hasFeedback={true}>
          {this.props.form.getFieldDecorator("title", titleRules)
            (<Input disabled={this.props.disabled} placeholder="Enter a name for your byte" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="Cover:">
          <Dragger
            name="cover"
            action="http://localhost:3000/bytes/cover/upload"
            onChange={this.handleUpload}
            headers={this.getHeaders()}
            showUploadList={false}
          >
            {this.getImageThumbnail()}
          </Dragger>
        </FormItem>
        <FormItem {...formItemLayout} label="Tags:" hasFeedback={true} className="tags-input">
          {this.props.form.getFieldDecorator("tags", {
            trigger: "onTagsChanged",
            getValueFromEvent: (newTags: string[]) => newTags
          })
            (<TagInput disabled={this.props.disabled} />)}
        </FormItem>
        <FormItem>
          <div className="prev-next-nav">
            <Button type="primary" className="prev" size="large" onClick={this.props.goBack}>
              <Icon type="left" />Back
            </Button>
            <Button
              type="primary"
              className="next"
              htmlType="submit"
              size="large"
              disabled={this.props.disabled}
              onClick={this.onSubmitHandler}
            >
              Save<Icon type="save" />
            </Button>
          </div>
        </FormItem>
      </Form>
    );
  }

  private handleUpload = (info: any) => {
    if (info.file.status === "done") {
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, (imageObj: any) =>
        this.setState({ imageObj, imageFilename: info.file.response.filename }));
    }
  }

  private getBase64(img: any, callback: any) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  private onSubmitHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((error: any, values: any) => {
      if (!error && this.props.saveBite) {
        this.props.saveBite({
          ...this.props.bite,
          imageFilename: this.state.imageFilename,
          title: values.title,
          tags: values.tags,
        });
      }
    });
  }

  private getHeaders() {
    let headers = {};
    const accessToken = localStorage.getItem("access_token");
    if (accessToken) {
      headers = { Authorization: "Bearer " + localStorage.getItem("access_token") };
    }
    return headers;
  }

  private getImageThumbnail() {
    if (this.state.imageObj ||
      (this.props.bite && this.props.bite.imageFilename && this.props.bite.imageFilename.length > 0)) {
      return (
        <img
          src={this.state.imageObj ||
            `http://localhost:3000/media/covers/${this.props.bite.imageFilename}`}
          alt=""
          className="cover"
        />
      );
    } else {
      return (
        < div >
          <p className="ant-upload-drag-icon">
            <Icon type="picture" />
          </p>
          <p className="ant-upload-text">Click or drag file to this area to upload</p>
          <p className="ant-upload-hint">Only one single image</p>
        </div >);
    }
  }
}

const WrapperNewByteForm = Form.create<IMetaDataFormProps>({
  mapPropsToFields(props: IMetaDataFormProps) {
    return {
      youtubeUrl: Form.createFormField({ value: props.bite ? props.bite.audioFilename : undefined }),
      tags: Form.createFormField({ value: props.bite ? props.bite.tags : undefined }),
      title: Form.createFormField({ value: props.bite ? props.bite.title : undefined }),
    };
  },
})(MetaDataForm);

export default withRouter(WrapperNewByteForm) as any;
