import { Button, Icon, Upload } from "antd";
import { UploadChangeParam } from "antd/lib/upload";
import { UploadFile } from "antd/lib/upload/interface";
import React from "react";
import { HttpStatus } from "soundbites-shared";

export interface IUploadProps {
  status?: HttpStatus;
  name?: string;
  url?: string;
  onUploadSuccess: (filename: string, originalFilename: string) => any;
}

export default class BiteUpload extends React.Component<IUploadProps, { fileList: UploadFile[] }> {
  public constructor(props: IUploadProps, state: { fileList: any[] }) {
    super(props, state);
    this.state = {
      fileList: []
    };
  }
  public render() {
    const props = {
      name: "file",
      action: "http://localhost:3000/bytes/upload",
      onChange: this.handleChange,
      headers: this.getHeaders()
    };
    return (
      <Upload {...props} fileList={this.state.fileList}>
        <Button>
          <Icon type="upload" /> upload
            </Button>
      </Upload>
    );
  }

  private handleChange = (info: UploadChangeParam, updateState?: boolean) => {
    let fileList = info.fileList;
    fileList = fileList.slice(-1);
    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        file.url = file.response.url;
      }
      return file;
    });

    if (info.file.response && info.file.status === "done") {
      this.props.onUploadSuccess(info.file.response.filename, info.file.name);
    }

    this.setState({ fileList });
  }

  private getHeaders() {
    let headers = {};
    const accessToken = localStorage.getItem("access_token");
    if (accessToken) {
      headers = { Authorization: "Bearer " + localStorage.getItem("access_token") };
    }
    return headers;
  }

}
