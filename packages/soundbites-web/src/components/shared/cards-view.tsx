import { Pagination, Row } from "antd";
import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { IBite } from "soundbites-shared";
import { BiteCard, CardButton } from "./bite-card";

export interface ICardViewProps extends RouteComponentProps<{ page: string }> {
  sort: string;
  loadBites: (sort: string, page: number) => Promise<{ data: IBite[], metadata: any }>;
  buttons?: CardButton[];
  deleteBite?: (biteId: string) => any;
  onNoBites?: () => any;
}
export interface ICardViewState {
  bites?: IBite[];
  total?: number;
  err: string;
}

export class CardsView extends React.PureComponent<ICardViewProps, ICardViewState> {
  constructor(props: ICardViewProps, state: ICardViewState) {
    super(props, state);
    this.state = { bites: undefined, err: undefined };
  }

  public componentWillMount() {
    // wip
    const page = this.props.match.params.page ? parseInt(this.props.match.params.page, undefined) : 1;
    this.doLoadBites(this.props.sort, page);
  }

  public componentWillReceiveProps(newProps: ICardViewProps) {
    if (newProps.match.params.page && newProps.match.params.page !== this.props.match.params.page) {
      this.doLoadBites(this.props.sort, parseInt(newProps.match.params.page, undefined));
    }
  }

  public render() {
    if (!this.state.bites) {
      return <div><p>Loading...</p></div>;
    }
    return (
      <div>
        <Row key="results" gutter={{ xs: 12, lg: 24, xl: 24, sm: 12, md: 12 }} >{this.renderBites()}</Row>
        <Row key="pagination">
          <Pagination
            onChange={this.handlePagination}
            defaultCurrent={this.props.match.params.page ? parseInt(this.props.match.params.page, undefined) : 1}
            total={this.state.total}
            defaultPageSize={12}
          />
        </Row>
      </div>
    );
  }
  public handleCardButtonClick = (button: CardButton, byte: IBite) => {
    switch (button) {
      case CardButton.Delete:
        this.props.deleteBite(byte._id);
        break;
      case CardButton.Download:
        window.open(`http://localhost:3000/media/${byte.audioFilename}`);
        break;
      case CardButton.Edit:
        this.props.history.push(`/${this.buildUrl(byte._id)}/edit`);
        break;
    }
  }

  public renderBites() {
    return this.state.bites.map((byte, i) =>
      <BiteCard {...this.props} onButtonClick={this.handleCardButtonClick} key={i} bite={byte} />);
  }

  private doLoadBites(sort: string, page: number) {
    this.props.loadBites(sort, page)
      .then((response) => {
        if (response.data && response.data.length < 1) {
          this.props.onNoBites();
        }
        this.setState({
          ...this.state,
          bites: response.data,
          total: response.metadata.total
        });
      })
      .catch((err) => this.setState({err}));
  }

  private buildUrl(param: string): string {
    const path = this.props.match.path;
    return path.replace(":page?", param);
  }

  private handlePagination = (page: number) => this.props.history.push(this.buildUrl(page.toString()));
}

export default withRouter(CardsView);
