import { Button, Input, Tag, Tooltip } from "antd";
import * as React from "react";
import "./metadata.less";

export interface ITagInputState {
  inputValue: "";
  inputVisible: boolean;
}
export interface ITagInputProps {
  onTagsChanged?: (newTags: string[]) => any;
  disabled: boolean;
  value?: string[];
}
export class TagInput extends React.Component<ITagInputProps, ITagInputState> {
  public input: HTMLElement;
  public constructor(props: any, state: ITagInputState) {
    super(props, state);
    this.state = {
      inputValue: "",
      inputVisible: false,
    };
  }

  public handleClose = (removedTag: string) => {
    const tags = this.props.value.filter((tag) => tag !== removedTag);
    this.handleOnTagsChanged(tags);
  }

  public showInput = () => {
    this.setState({ ...this.state, inputVisible: true }, () => this.input.focus());
  }

  public handleInputChange = (e: React.FormEvent<any>) => {
    this.setState({ inputValue: e.currentTarget.value });
  }

  public handleInputConfirm = (e: any) => {
    const state = this.state;
    const inputValue = state.inputValue;
    let tags = this.props.value;
    if (!tags) {
      tags = [];
    }
    if (inputValue && tags.indexOf(inputValue) === -1) {
      tags = [...tags, inputValue];
    }
    this.handleOnTagsChanged(tags);
    this.setState({
      inputValue: "",
      inputVisible: false,
    });
  }

  public handleOnTagsChanged = (tags: string[]) => {
    if (this.props.onTagsChanged) {
      this.props.onTagsChanged(tags);
    }
  }

  public saveInputRef = (input: any) => this.input = input;

  public render() {
    const { inputVisible, inputValue } = this.state;
    const tags = this.props.value || [];
    return (
      <div>
        {tags.map((tag, index) => {
          const isLongTag = tag.length > 20;
          const tagElem = (
            // tslint:disable-next-line:jsx-no-lambda
            <Tag key={tag} closable={!this.props.disabled} afterClose={() => this.handleClose(tag)}>
              {isLongTag ? `${tag.slice(0, 20)}...` : tag}
            </Tag>
          );
          return isLongTag ? <Tooltip title={tag}>{tagElem}</Tooltip> : tagElem;
        })}
        {inputVisible && (
          <Input
            ref={this.saveInputRef}
            type="text"
            size="small"
            style={{ width: 78 }}
            value={inputValue}
            onChange={this.handleInputChange}
            onBlur={this.handleInputConfirm}
            onPressEnter={this.handleInputConfirm}
          />
        )}
        {!inputVisible && <Button size="small" type="dashed" onClick={this.showInput}>+ New Tag</Button>}
      </div>
    );
  }
}
