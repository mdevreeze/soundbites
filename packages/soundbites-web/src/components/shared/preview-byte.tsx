import { Button } from "antd";
import { Howl } from "howler";
import React from "react";
import { HttpStatus } from "soundbites-shared";

export interface IPreviewByteProps {
  audioUrl?: string;
  processingStatus?: HttpStatus;
  errors?: any[];
}

export class PreviewByte extends React.Component<IPreviewByteProps, { playing: boolean }> {
  private howl: Howl;
  public constructor(props: IPreviewByteProps, state: { playing: boolean }) {
    super(props, state);
    if (!this.state) {
      this.state = {
        playing: false
      };
    }
  }

  public render() {
    if (this.props.audioUrl) {
      if (!this.howl) {
        this.howl = new Howl({
          src: `${process.env.API_HOST}/media/audio/${this.props.audioUrl}`,
          format: "mp3",
          onend: () => this.setState({ playing: false })
        });
      }
      if (this.state.playing) {
        return (
          <div>
            <Button icon="stop-circle" onClick={this.onStopClickHandler}>Stop</Button>
          </div>
        );
      }
      return (
        <div>
          <Button icon="play-circle" onClick={this.onPlayClickHandler}>Play</Button>
        </div>
      );
    } else {
      return (
        <div>
          Loading...
      </div>
      );
    }
  }
  private onPlayClickHandler = () => {
    this.howl.play();
    this.setState({ playing: this.howl.playing() });
  }

  private onStopClickHandler = () => {
    this.howl.stop();
    this.setState({ playing: this.howl.playing() });
  }
}

export default PreviewByte;
