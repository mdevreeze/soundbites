// import { Button, Icon } from "antd";
import { Alert, Layout } from "antd";
import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { DraftContext, HttpStatus, IBite, IDraftStore } from "soundbites-shared";
const { Content } = Layout;
import BiteUpload from "../shared/bite-upload";
import MetaDataForm from "../shared/metadata-form";
import { PreviewByte } from "../shared/preview-byte";

export interface IMetaDataViewProps extends RouteComponentProps<{ id: string }> {
  audioUrl?: string;
  biteLoaded?: boolean;
  enableUpload: boolean;
  bite?: IBite;
  saveBite?: (newByte: IBite) => Promise<IBite>;
  loadBite?: (biteId: string) => any;
  processingStatus?: HttpStatus;
  savingStatus?: HttpStatus;
  onUploadedSuccess?: (filename: string, originalFilename: string) => any;
  error: string;
}

export class MetaDataView extends React.Component<IMetaDataViewProps, undefined> {
  public constructor(props: IMetaDataViewProps) {
    super(props);
  }

  public componentWillMount() {
    if (this.props.loadBite && this.props.match.params.id) {
      this.props.loadBite(this.props.match.params.id);
    }
  }

  public render() {
    return (
      <Content className="content">
        <Layout style={{ padding: "24px 0", background: "#fff" }}>
          <Content style={{ padding: "0 24px", minHeight: 280 }}>
            {this.props.error &&
              // TODO move error handeling to some generic component and load that one instead (maybe even from de draft provider)
              <Alert type="error" message={`Somthing went wrong (${this.props.error})`} showIcon={true} />
            }
            <div className="ant-row">
              <h2>Save your byte</h2>
              <p className="intro-text">
                Enter additional information about your clip so other users will be able to find it.</p>
            </div>
            <div className="ant-row">
              {this.props.enableUpload && <div className="">
                <BiteUpload onUploadSuccess={this.props.onUploadedSuccess} />
              </div>}
              <div className="">
                <PreviewByte audioUrl={this.props.audioUrl} />
              </div>
            </div>
            <div className="ant-row">
              <MetaDataForm
                bite={this.props.bite}
                goBack={this.props.history.goBack}
                disabled={this.props.savingStatus !== HttpStatus.NotStarted}
                saveBite={this.handleSaveByte}
              />
            </div>
          </Content>
        </Layout>
      </Content>
    );
  }

  private handleSaveByte = (byte: IBite) => {
    if (this.props.saveBite) {
      this.props.saveBite(byte);
    }
  }
}

export default withRouter((props: RouteComponentProps<IMetaDataViewProps> & IMetaDataViewProps) => (
  <DraftContext.Consumer>
    {(context: IDraftStore) =>
      <MetaDataView
        {...props}
        enableUpload={false}
        audioUrl={context.draft.audioFilename}
        bite={context.draft}
        biteLoaded={context.draft !== undefined}
        savingStatus={context.processingStatus}
        saveBite={context.actions.saveDraft}
        error={context.error}
      />
    }
  </DraftContext.Consumer>
  )
);
