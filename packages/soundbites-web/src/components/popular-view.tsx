import { Layout } from "antd";
import React from "react";
import { withRouter } from "react-router-dom";
import { fetchBites } from "soundbites-shared";
import CardView, { ICardViewProps } from "./shared/cards-view";
const { Content } = Layout;

export class PopularView extends React.PureComponent<ICardViewProps, any> {
  public render() {
    return (
      <Content className="content">
        <Layout style={{ padding: "24px 0", background: "#fff" }}>
          <Content style={{ padding: "0 24px", minHeight: 280 }}>
            <CardView sort="popular" loadBites={fetchBites} />
          </Content>
        </Layout>
      </Content>
    );
  }
}

export default withRouter(PopularView);
