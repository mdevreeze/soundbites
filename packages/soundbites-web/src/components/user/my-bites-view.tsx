import { Layout } from "antd";
import React from "react";
import { withRouter } from "react-router-dom";
import { fetchUserBites } from "soundbites-shared";
import { CardButton } from "../shared/bite-card";
import CardView, { ICardViewProps } from "../shared/cards-view";
const { Content } = Layout;

export class MyBytesView extends React.Component<ICardViewProps, { hasBites: boolean }> {
  constructor(props: ICardViewProps, state: { hasBites: boolean }) {
    super(props, state);
    this.state = { hasBites: undefined};
  }
  public render() {
    if (!this.state.hasBites === false) {
      return <div><p>You don't have any bytes</p></div>;
    } else {
      return (
        <Content className="content">
          <Layout style={{ padding: "24px 0", background: "#fff" }}>
            <Content style={{ padding: "0 24px", minHeight: 280 }}>
              <CardView
                sort="new"
                buttons={[CardButton.Delete, CardButton.Download, CardButton.Edit]}
                loadBites={this.loadBites}
              />
            </Content>
          </Layout>
        </Content>);
    }
  }

  private loadBites = (): Promise<any> => {
    return fetchUserBites().then((resp) => resp).catch((err) => err);
  }
}
export default withRouter(MyBytesView);
