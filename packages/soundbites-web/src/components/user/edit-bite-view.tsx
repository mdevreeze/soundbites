import { Layout } from "antd";
import React from "react";
import { RouteComponentProps } from "react-router";
import { withRouter } from "react-router-dom";
import { DraftContext, HttpStatus, IBite, IDraftStore } from "soundbites-shared";
const { Content } = Layout;
import MetaDataForm from "../shared/metadata-form";

export interface IEditBiteViewProps extends RouteComponentProps<{ id: string }> {
  bite?: IBite;
  saveBite?: (newBite: IBite) => any;
  loadBite?: (biteId: string) => any;
  savingStatus?: HttpStatus;
}

export class EditByteView extends React.Component<IEditBiteViewProps> {
  public componentWillMount() {
    if (this.props.loadBite) {
      this.props.loadBite(this.props.match.params.id);
    }
  }

  public render() {
    return (
      <Content className="content">
        <Layout style={{ padding: "24px 0", background: "#fff" }}>
          <Content style={{ padding: "0 24px", minHeight: 280 }}>
            <div className="ant-row">
              <h2>Save your byte</h2>
              <p className="intro-text">
                Enter additional information about your clip so other users will be able to find it.</p>
            </div>
            <div className="ant-row">
              <MetaDataForm
                bite={this.props.bite}
                goBack={this.props.history.goBack}
                disabled={this.props.savingStatus !== HttpStatus.NotStarted}
                saveBite={this.props.saveBite}
              />
            </div>
          </Content>
        </Layout>
      </Content>
    );
  }
}

export default withRouter((props: RouteComponentProps<IEditBiteViewProps> & IEditBiteViewProps) => (
  <DraftContext.Consumer>
    {(context: IDraftStore) =>
      <EditByteView
        {...props}
        bite={context.draft}
      />
    }
  </DraftContext.Consumer>
  )
);
