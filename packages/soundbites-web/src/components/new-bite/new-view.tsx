import { Alert } from "antd";
import { FormProps } from "antd/lib/form";
import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { AuthUserObj } from "soundbites-shared";
import { DraftContext, IDraftStore } from "soundbites-shared";
import UploadForm from "./upload-form";
import YoutubeStartForm from "./youtube-start-form";
const newView = (props: RouteComponentProps<any> & FormProps) => {
  const onSubmitYoutube = (youtubeCode: string) => props.history.push(`/new/clipper/${youtubeCode}`);
  const onSubmitFile = () => props.history.push(`/new/metadata`);
  return (
    <DraftContext.Consumer>
      {(context: IDraftStore) =>
        <div>
          {!AuthUserObj.isAuthenticated() && <Alert
            type="error"
            showIcon={true}
            message="You're not logged in"
            description={(<span>Click <a href="#" onClick={AuthUserObj.login}>here</a> to login.</span>)}
          />}
          <h2>Create a new mediabyte</h2>
          <div>
            <h3>Create a new bite from Youtube</h3>
            <div>
              <p className="intro-text">
                To create a new byte: provide the Youtube URL of the audio byte you want to sample.
              </p>
              <YoutubeStartForm
                onSubmitClick={onSubmitYoutube}
                setYoutubeCode={context.actions.setYoutubeCode}
                youtubeCode={context.draft.originReference}
              />
            </div>
          </div>
          <br />
          <hr />
          <br />
          <h3>Upload new bite</h3>
          <p className="intro-text">Upload a mp3 file to create a new bite</p>
          <UploadForm
            onSubmitClick={onSubmitFile}
            onUploadSuccess={context.actions.onFileUploadSuccess}
            filename={context.draft.originReference}
          />
        </div>
      }
    </DraftContext.Consumer>
  );
};

export default withRouter(newView);
