import { Breadcrumb, Layout, } from "antd";
import React from "react";
import { Route, withRouter } from "react-router-dom";
import { DraftProvider } from "soundbites-shared";
import MetaDataView from "../shared/metadata-view";
import NewView from "./new-view";
import SuccessView from "./succes-view";
import ClipperView from "./youtube-clipper";
const { Content } = Layout;
const newByteLayout = () => (
    <Content className="content" style={{ padding: "0 50px" }}>
        <Breadcrumb style={{ margin: "12px 0" }}>
            <Breadcrumb.Item>New byte</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb>
        <Layout style={{ padding: "24px 0", background: "#fff" }}>
          <DraftProvider>
            <Content style={{ padding: "0 24px", minHeight: 280 }}>
                {/* <Route
                    path="/new/youtube"
                    component={YoutubeView}
                /> */}
                <Route
                    path="/new/clipper/:youtubeCode"
                    component={ClipperView}
                />
                <Route
                    path="/new/metadata"
                    component={MetaDataView}
                />
                <Route
                    path="/new/success/:id"
                    component={SuccessView}
                />
                <Route
                    exact={true}
                    path="/new"
                    component={NewView}
                />
            </Content>
            </DraftProvider>
        </Layout>
    </Content>
);

export default withRouter(newByteLayout);
