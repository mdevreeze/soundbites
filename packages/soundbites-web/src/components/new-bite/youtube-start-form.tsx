import { Button, Form, Icon, Input } from "antd";
import { FormComponentProps } from "antd/lib/form";
import React from "react";
const FormItem = Form.Item;
const youtTubeUrlRules = {
  rules: [
    {
      message: "Not a valid YouTube URL!",
      type: "url",
    },
    {
      message: "Enter a YouTube URL",
      required: true,
    },
    {
      message: "Not a valid YouTube Video URL!",
      // tslint:disable-next-line:max-line-length
      pattern: /^(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:youtube\.com|youtu.be)\/(?:[\w\-]+\?v=|embed\/|v\/)?[\w\-]+\S+?$/
    }
  ],
};

export interface IYoutubeStartForm {
  youtubeCode: string;
  setYoutubeCode: (url: string) => any;
  onSubmitClick: (youtubeCode: string) => any;
}
export class YoutubeStartForm extends React.Component<IYoutubeStartForm & FormComponentProps, any> {
  public constructor(props: IYoutubeStartForm & FormComponentProps, state: { youtubeUrl: string }) {
    super(props, state);
  }

  public render() {
    return (
      <Form>
        <FormItem
          labelCol={{ sm: 6, xs: 24 }}
          wrapperCol={{ sm: 10, xs: 24 }}
          label="YouTube URL:"
          hasFeedback={true}
        >
          {this.props.form.getFieldDecorator("youtubeUrl", youtTubeUrlRules)
            (<Input
              prefix={<Icon type="link" />}
              placeholder="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
            />)}
        </FormItem>
        <div style={{ textAlign: "right" }}>
          <Button
            type="primary"
            className="next"
            htmlType="submit"
            size="large"
            onClick={this.onSubmit}
          >
            Create from Youtube<Icon type="right" />
          </Button>
        </div>
      </Form>
    );
  }
  private onSubmit = () => this.props.onSubmitClick && this.props.onSubmitClick(this.props.youtubeCode);
}

const YoutubeStartFormWrapper = Form.create({
  mapPropsToFields(props: IYoutubeStartForm) {
    return {
      youtubeUrl: Form.createFormField(
        { value: props.youtubeCode ? `https://www.youtube.com/watch?v=${props.youtubeCode}` : "" }
      )
    };
  },
  onFieldsChange(props: IYoutubeStartForm, values: any) {
    const youtubeUrl = values.youtubeUrl;
    if (youtubeUrl && youtubeUrl.validating !== true && youtubeUrl.dirty !== true && !youtubeUrl.errors) {
      props.setYoutubeCode(youtubeUrl.value);
    }
  }
})(YoutubeStartForm);

export default YoutubeStartFormWrapper;
