import { Col, InputNumber, Row, Slider } from "antd";
import React from "react";

export interface IRangeSliderProps {
  onChange?: (value: [number, number]) => any;
  value?: [number, number];
  maxValue?: number;
}

export class RangeSlider extends React.Component<IRangeSliderProps, any> {
  public static defaultProps = {
    maxValue: 100,
    onChange: undefined
  } as IRangeSliderProps;

  public constructor(props: IRangeSliderProps) {
    super(props);
  }
  public shouldComponentUpdate(nextProps: Readonly<IRangeSliderProps>, nextState: Readonly<any>): boolean {
    if (!this.props.value || !nextProps.value && this.props.value || nextProps.value) {
      return true;
    }
    return this.props.maxValue !== nextProps.maxValue
      // && this.props.onChange === nextProps.onChange
      || (this.props.value[0] !== nextProps.value[0] || this.props.value[1] !== nextProps.value[1]);
  }

  public render() {
    const start = this.props.value[0];
    const stop = this.props.value[1];
    let range: [number, number] = [0, 0];
    if (start && stop) {
      range = [parseFloat(start.toString()), parseFloat(stop.toString())];
    // tslint:disable-next-line:triple-equals
    } else if (stop != 0) {
      range = [0.00, parseFloat(stop.toString())];
    // tslint:disable-next-line:triple-equals
    } else if (start != 0) {
      range = [parseFloat(start.toString()), this.props.maxValue];
    }
    return (
      <Row>
        <Col span={4}>
          <InputNumber
            min={0}
            max={this.props.maxValue}
            style={{ width: "100px" }}
            step={0.10}
            value={start}
            onChange={this.onChangeStartText}
          />
        </Col>
        <Col span={16}>
          <Slider
            range={true}
            min={0}
            max={this.props.maxValue}
            onChange={this.onChange}
            value={range}
            step={0.25}
          />
        </Col>
        <Col span={4} style={{ textAlign: "right" }}>
          <InputNumber
            min={0}
            max={this.props.maxValue}
            style={{ width: "100px" }}
            step={0.10}
            value={stop}
            onChange={this.onChangeEndText}
          />
        </Col>
      </Row>
    );
  }
  private onChangeStartText = (value: number) => this.onChangeText(value, 0);
  private onChangeEndText = (value: number) => this.onChangeText(value, 1);
  private onChangeText(value: number, index: number) {
    const newValue = this.props.value;
    newValue[index] = value;

    if (this.props.onChange) {
      this.props.onChange(newValue);
    }
  }

  private onChange = (value: [number, number]) => {
    if (this.props.onChange) {
      this.props.onChange(value);
    }
  }
}
