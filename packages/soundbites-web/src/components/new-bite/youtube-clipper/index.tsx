import { Button, Icon } from "antd";
import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { DraftContext, IDraftStore } from "soundbites-shared";
import YoutubeClipper from "./youtube-clipper";

export interface IClipperProps extends RouteComponentProps<{ youtubeCode: string }> {
  youtubeCode: string;
  startClippingYoutube: () => any;
}

class YoutubeClipperView extends React.Component<IClipperProps, {clipper: any}> {
  public constructor(props: IClipperProps) {
    super(props);
  }

  public render() {
    return (
      <div>
        <div className="ant-row">
          <h2>Clip your byte</h2>
          <p className="intro-text">
            Clip the part of the youtube video of which you want to create a byte from.</p>
        </div>
        <div className="ant-row">
          <DraftContext.Consumer>
            {(context: IDraftStore) =>
              <YoutubeClipper
                setVideoRange={context.actions.setYoutubeRange}
                range={context.draft.originVideoRange}
                youtubeId={this.props.match.params.youtubeCode}
              />
            }
          </DraftContext.Consumer>
        </div>
        <div className="ant-row">
          <div className="prev-next-nav">
            <Button type="primary" className="prev" size="large" onClick={this.clickBack}>
              <Icon type="left" />Back
            </Button>
            <Button type="primary" className="next" size="large" onClick={this.clickNext}>
              Next<Icon type="right" />
            </Button>
          </div>
        </div>
      </div>
    );
  }
  private clickNext = () => {
    if (this.props.startClippingYoutube) {
      this.props.startClippingYoutube();
    }
    // console.log(this.clipper);
    // this.clipper.getWrappedInstance().unMountResources();

    this.props.history.push("/new/metadata");
  }

  private clickBack = () => this.props.history.push("/new/youtube");
}

export default withRouter((props) => (
  <DraftContext.Consumer>
    {(context: IDraftStore) =>
      <YoutubeClipperView
        {...props}
        startClippingYoutube={context.actions.doYoutubeClipping}
        youtubeCode={context.draft.originReference}
      />
    }
  </DraftContext.Consumer>
  )
);
