import { Button, Col, Row } from "antd";
import React from "react";
import YouTube from "react-youtube";
import { RangeSlider } from "./rangeSlider";

export interface IYoutubeClipperProps {
  youtubeId: string;
  setVideoRange: (range: [number, number]) => any;
  range: [number, number];
}
export interface IYoutubeClipperState {
  player: YT.Player;
  currentTime: number;
  unMounted: boolean;
}

class YoutubeClipper extends React.Component<IYoutubeClipperProps, IYoutubeClipperState> {
  private timeUpdateInterval: any;
  private setRangeTimeOut: any;
  public constructor(props: IYoutubeClipperProps, state: IYoutubeClipperState) {
    super(props, state);
    this.state = {
      currentTime: 0.00,
      player: undefined,
      unMounted: false
    };
  }

  public shouldComponentUpdate(nextProps: Readonly<IYoutubeClipperProps>,
                               nextState: Readonly<IYoutubeClipperState>): boolean {
    if (!this.props.range || !nextProps.range && this.props.range || nextProps.range) {
      return true;
    }

    const shouldUpdate = (this.props.range[0] !== nextProps.range[0] || this.props.range[1] !== nextProps.range[1])
      // && this.props.onChange === nextProps.onChange
      || this.state.currentTime !== nextState.currentTime;
    return shouldUpdate;
  }

  public render() {
    return (
      <div>
        <Row>
          <Col span={24} style={{ textAlign: "center" }}>
            <YouTube
              opts={{ rel: 0 }}
              videoId={this.props.youtubeId}
              onReady={this.setPlayer}
              onPlay={this.onPlay}
              onPause={this.stopUpdatingCurrentTime}
              onEnd={this.stopUpdatingCurrentTime}
            />
          </Col>
        </Row>
        <Row style={{ marginBottom: "5px" }}>
          <Col span={4}>
            <Button
              icon="verticle-right"
              style={{ width: "100px" }}
              onClick={this.setStart}
            >
              Set start
            </Button>
          </Col>
          <Col span={2} offset={7}>
            <p style={{ lineHeight: "28px" }}>
              {this.state.player
                ? this.state.currentTime + "/" + Math.ceil(this.state.player.getDuration())
                : "Loading..."}
            </p>
          </Col>
          <Col span={4} offset={7} style={{ textAlign: "right" }}>
            <Button
              icon="verticle-left"
              style={{ width: "100px" }}
              onClick={this.setEnd}
            >
              Set end
            </Button>
          </Col>
        </Row>
        <RangeSlider
          maxValue={this.state.player ? this.state.player.getDuration() : 100}
          value={this.props.range ? this.props.range : [0.01, 0.00]}
          onChange={this.setRange}
        />

      </div>
    );
  }
  public componentWillUnmount() {
    this.unMountResources();
  }

  public unMountResources() {
    this.setState({ ...this.state, unMounted: true });
    this.stopUpdatingCurrentTime();
    this.stopRangeTimeOut();
  }

  public stopRangeTimeOut = () => clearTimeout(this.setRangeTimeOut);

  public stopUpdatingCurrentTime = () => { clearInterval(this.timeUpdateInterval); return {}; };

  private onPlay = () => {
    if (this.props.range[0] !== 0 && this.state.player.getCurrentTime() < this.props.range[0]) {
      this.state.player.seekTo(this.props.range[0], true);
    }

    this.stopUpdatingCurrentTime();
    const intervalId = setInterval(() => {
      if (this.props.range[1] !== 0 && this.state.player.getCurrentTime() > this.props.range[1]) {
        this.state.player.seekTo(this.props.range[0], true);
        clearInterval(intervalId);
      }

      this.setState({
        ...this.state,
        currentTime: Math.ceil(this.state.player.getCurrentTime()),
      });
    }, 200);
    this.timeUpdateInterval = intervalId;
    return {};
  }

  private setStart = () => {
    const range = this.props.range;
    range[0] = Math.ceil(this.state.player.getCurrentTime() * 100) / 100;
    this.setRange(range);
  }
  private setEnd = () => {
    const range = this.props.range;
    range[1] = Math.ceil(this.state.player.getCurrentTime() * 100) / 100;
    this.setRange(range);
  }
  private setPlayer = (event: YT.PlayerEvent) => {
    // tslint:disable-next-line:triple-equals
    if (!this.props.range || (this.props.range[0] == 0 && this.props.range[1] == 0)) {
      this.props.setVideoRange([0.00, event.target.getDuration()]);
    }

    this.setState({ ...this.state, player: event.target });
  }
  private setRange = (range: [number, number]) => {
    const setRangeTimeOut = setTimeout(() => {
      if (this.state.player.getPlayerState() !== YT.PlayerState.PLAYING) {
        this.state.player.seekTo(range[0], true);
      } else if (this.state.player.getCurrentTime() < range[0]) {
        this.state.player.seekTo(range[0], true);
      }
      clearTimeout(this.setRangeTimeOut);
    }, 500);
    this.setRangeTimeOut = setRangeTimeOut;
    if (this.props.setVideoRange) {
      this.props.setVideoRange(range);
    }
  }
}

export default YoutubeClipper;
