import { Button, Form, Icon } from "antd";
import { FormComponentProps } from "antd/lib/form";
import React from "react";
import BiteUpload from "../shared/bite-upload";
const FormItem = Form.Item;
export interface IUploadFormProps {
  filename?: any;
  onSubmitClick: () => any;
  onUploadSuccess: (filename: string, originalFilename: string) => any;
}
export class UploadForm extends React.Component<IUploadFormProps & FormComponentProps, any> {
  public constructor(props: IUploadFormProps & FormComponentProps) {
    super(props);
  }

  public render() {
    return (
      <Form>
        <FormItem
          labelCol={{ sm: 6, xs: 24 }}
          wrapperCol={{ sm: 10, xs: 24 }}
          label="Upload file"
          hasFeedback={true}
        >
          {this.props.form.getFieldDecorator("filename", )
            (<BiteUpload onUploadSuccess={this.onUploadHandler}  />)}
        </FormItem>
        <div style={{ textAlign: "right" }}>
          <Button
            type="primary"
            className="next"
            htmlType="submit"
            size="large"
            onClick={this.onSubmit}
          >
            Create from file<Icon type="right" />
          </Button>
        </div>
      </Form>
    );
  }

  private onUploadHandler = (filename: string, originalFilename: string) => {
    if (this.props.onUploadSuccess) {
      this.props.onUploadSuccess(filename, originalFilename);
    }
  }

  private onSubmit = () => this.props.onSubmitClick && this.props.onSubmitClick();
}

const UploadFormWrapper = Form.create<IUploadFormProps>({
  mapPropsToFields(props: IUploadFormProps) {
    return {
      filename: Form.createFormField({
        ...props.filename,
        value: props.filename ? props.filename.value : ""
      })
    };
  },
  onFieldsChange(props: IUploadFormProps, values: any) {
    // const youtubeUrl = values.youtubeUrl;
  }
})(UploadForm);

export default UploadFormWrapper;
