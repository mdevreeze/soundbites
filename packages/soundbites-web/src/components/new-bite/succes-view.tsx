import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { DraftContext, IBite, IDraftStore } from "soundbites-shared";
import BiteCard from "../shared/bite-card";

export interface ISuccessComponentProps extends RouteComponentProps<{ id: string }> {
  bite: IBite;
  resetState: () => any;
}

class SuccessView extends React.Component<ISuccessComponentProps, any> {
  public render() {
    let bite = (<div>Loading...</div>);
    if (this.props.bite) {
      bite = <BiteCard bite={this.props.bite} />;
    }

    return (
      <div>
        <h2>Succesfully created your new byte</h2>
        {bite}
      </div>
    );
  }

  public componentWillUnmount() {
    this.props.resetState();
  }
}

export default withRouter((props) => (
    <DraftContext.Consumer>
    {(context: IDraftStore) =>
      <SuccessView {...props} bite={context.draft} resetState={context.actions.resetDraft} />
    }
    </DraftContext.Consumer>
  )
);
