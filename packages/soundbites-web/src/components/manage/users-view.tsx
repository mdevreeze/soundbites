import { Button, Layout, message, Table } from "antd";
import moment from "moment";
import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { fetchUsers, IUser } from "soundbites-shared";
const { Content } = Layout;

export class UsersView extends React.Component<RouteComponentProps<any>, { users: IUser[] }> {
  private columns = [
    { title: "Email", dataIndex: "email", key: "email" },
    // { title: "Name", dataIndex: "name", key: "Name" },
    { title: "UserId", dataIndex: "user_id", key: "user_id" },
    {
      title: "Last login",
      dataIndex: "last_login",
      key: "last_login",
      render: (text: any, row: any) => <span key="last_login">{moment(row.last_login).format("LLL")}</span>
    },
    {
      title: "Created at",
      dataIndex: "created_at",
      key: "created_at",
      render: (text: any, row: any) => <span key="created_at">{moment(row.created_at).format("LLL")}</span>
    },
    {
      title: "Actions",
      key: "actions",
      render: (text: any, row: any) => (
        <span>
          <Button shape="circle" icon="switcher" />
          <Button shape="circle" icon="edit" style={{ margin: "0 5px" }} />
          <Button shape="circle" icon="delete" />
        </span>
      )
    }
  ];

  constructor(props: RouteComponentProps<any>, state: { users: IUser[] }) {
    super(props, state);
    this.state = { users: undefined };
  }

  public componentWillMount() {
    fetchUsers().then((value) => {
      this.setState({ ...this.state, users: value.data as IUser[]});
    })
      .catch((err) => {
        message.success(`Error loading users: ${err}`);
      });
  }

  public render() {
    return (
      <Content className="content">
        <Layout style={{ padding: "24px 0", background: "#fff" }}>
          <Content style={{ padding: "0 24px", minHeight: 280 }}>
            <Table dataSource={this.state.users} columns={this.columns} />
          </Content>
        </Layout>
      </Content>);
  }
}

export default withRouter(UsersView);
