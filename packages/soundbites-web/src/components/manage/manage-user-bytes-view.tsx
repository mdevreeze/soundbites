import { Layout } from "antd";
import React from "react";
import { RouteComponentProps } from "react-router";
import { withRouter } from "react-router-dom";
import { fetchUserBites } from "soundbites-shared";
import { CardButton } from "../shared/bite-card";
import CardView, { ICardViewProps } from "../shared/cards-view";
const { Content } = Layout;
export interface IManageUserBitesViewProps extends RouteComponentProps<{ userId: string }> {
  username: string;
  userEmail: string;
}

export class ManageUserBitesView
  extends React.Component<ICardViewProps & IManageUserBitesViewProps, { hasBites: boolean }> {
  constructor(props: ICardViewProps & IManageUserBitesViewProps, state: { hasBites: boolean }) {
    super(props, state);
    this.state = { hasBites: undefined};
  }

  public render() {
    if (!this.state.hasBites === false) {
      return <div><p>User {this.props.username} doesn't has any bytes.</p></div>;
    } else {
      return (
        <Content className="content">
          <Layout style={{ padding: "24px 0", background: "#fff" }}>
            <Content style={{ padding: "0 24px", minHeight: 280 }}>
              <h1>Bites of {this.props.username}</h1>
              <h2>{this.props.userEmail} # {this.props.match.params.userId}</h2>
              <CardView
                loadBites={this.loadBites}
                sort="new"
                buttons={[CardButton.Delete, CardButton.Download, CardButton.Edit]}
              />
            </Content>
          </Layout>
        </Content>);
    }
  }
  private loadBites = (): Promise<any> => {
    return fetchUserBites(this.props.username).then((r) => r).catch((e) => e);
  }
}

export default withRouter(ManageUserBitesView);
