import { Layout } from "antd";
import React from "react";
import { withRouter } from "react-router-dom";
import { fetchBites } from "soundbites-shared";
import { CardButton } from "../shared/bite-card";
import CardView, { ICardViewProps } from "../shared/cards-view";
const { Content } = Layout;

export class ManageBitesView extends React.Component<ICardViewProps, any> {
  public render() {
    return (
      <Content className="content">
        <Layout style={{ padding: "24px 0", background: "#fff" }}>
          <Content style={{ padding: "0 24px", minHeight: 280 }}>
            <CardView
              sort="new"
              buttons={[CardButton.Delete, CardButton.Download, CardButton.Edit]}
              loadBites={fetchBites}
            />
          </Content>
        </Layout>
      </Content>);
  }
}

export default withRouter(ManageBitesView);
