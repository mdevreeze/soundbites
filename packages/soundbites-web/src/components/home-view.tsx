import { Breadcrumb, Layout } from "antd";
import * as React from "react";
const { Content } = Layout;
const home = () => (
  <Content className="content">
    <Breadcrumb style={{ margin: "12px 0" }}>
      <Breadcrumb.Item>Home</Breadcrumb.Item>
      <Breadcrumb.Item>List</Breadcrumb.Item>
      <Breadcrumb.Item>App</Breadcrumb.Item>
    </Breadcrumb>
    <Layout style={{ padding: "24px 0", background: "#fff" }}>
      <Content style={{ padding: "0 24px", minHeight: 280 }}>
        Content
      </Content>
    </Layout>
  </Content>
);

export default home;
