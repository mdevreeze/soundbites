import { Col, Icon, Layout, Menu, Row } from "antd";
import React from "react";
import { AuthUserObj } from "soundbites-shared";
import home from "./components/home-view";
import newByteLayout from "./components/new-bite";
import popularView from "./components/popular-view";
import recentView from "./components/recent-view";

import { Link, Route, RouteComponentProps, withRouter } from "react-router-dom";
import ManageBitesView from "./components/manage/manage-bites-view";
import ManageUsersView from "./components/manage/users-view";
import EditBiteMetaData from "./components/shared/metadata-view";
import UserEditBiteView from "./components/user/edit-bite-view";
import MyBitesView from "./components/user/my-bites-view";

const { Header, Footer } = Layout;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
export interface ILayoutProps extends RouteComponentProps<any> {
  selectedPage?: string;
}

export class MbLayout extends React.PureComponent<ILayoutProps> {
  public render() {
    const isAdmin = AuthUserObj.userHasRoles(["admin"]);
    return (
      <Layout>
        <Header className="header">
          <Row>
            <Col span={2} className="logo"><h1><Link to="/">Soundbites</Link></h1></Col>
            <Col span={20}>
              <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={["1"]}
                selectedKeys={[this.props.selectedPage]}
                style={{ lineHeight: "64px" }}
              >
                <Menu.Item key="/home"><Link to="/home"><Icon type="home" />Home</Link></Menu.Item>
                <Menu.Item key="/popular"><Link to="/popular"><Icon type="star" />Popular</Link></Menu.Item>
                <Menu.Item key="/recent"><Link to="/recent"><Icon type="clock-circle" />Recent</Link></Menu.Item>
                {AuthUserObj.isAuthenticated() &&
                  (<Menu.Item key="/new"><Link to="/new"><Icon type="file" />New Byte</Link></Menu.Item>)}
                {AuthUserObj.isAuthenticated() &&
                  (<SubMenu key="/user" title={<span><Icon type="user" />User</span>} >
                    <MenuItemGroup>
                      <Menu.Item key="/user/profile"><Link to="/user/profile">Profile</Link></Menu.Item>
                      <Menu.Item key="/user/bytes"><Link to="/user/bytes">My Bytes</Link></Menu.Item>
                    </MenuItemGroup>
                  </SubMenu>)}
                {isAdmin &&
                  (<SubMenu key="manage" title={<span><Icon type="tool" />Manage</span>} >
                    <MenuItemGroup>
                      <Menu.Item key="/manage/bytes">
                        <Link to="/manage/bytes"><Icon type="file" />Manage Bytes</Link>
                      </Menu.Item>
                      <Menu.Item key="/manage/users">
                        <Link to="/manage/users"><Icon type="usergroup-delete" />Manage Users</Link>
                      </Menu.Item>
                    </MenuItemGroup>
                  </SubMenu>)
                }
              </Menu>
            </Col>
            {this.userLogin()}
          </Row>
        </Header>
        <Route path="/popular/:page?" component={popularView} />
        <Route path="/recent/:page?" component={recentView} />
        <Route path="/new" component={newByteLayout} />
        <Route path="/callback" component={this.handleAuthentication} />
        <Route path="/user/bytes/:page?" component={MyBitesView} />
        <Route path="/user/bytes/:id/edit" component={UserEditBiteView} />
        <Route exact={true} path="/manage/bytes/:page?" component={ManageBitesView} />
        <Route
          path="/manage/bytes/:id/edit"
          // tslint:disable-next-line:jsx-no-lambda
          render={(props: RouteComponentProps<any>) =>
            <EditBiteMetaData error={undefined} {...props} enableUpload={true}/>}
        />
        <Route exact={true} path="/manage/users" component={ManageUsersView} />
        <Route exact={true} path="/" component={home} />
        <Footer style={{ textAlign: "center" }}>Soundbytes © 2017 Fluxcode</Footer>
      </Layout>
    );
  }

  public userLogin() {
    if (AuthUserObj.isAuthenticated()) {
      return (
        <Col span={2} className="gray-1" style={{ textAlign: "right" }}>
          Welcome, <Link onClick={this.logout} to={"/logout"}>logout</Link>
        </Col>
      );
    }
    return (
      <Col span={2} style={{ textAlign: "right" }}>
        <Link onClick={this.login} to={"/login"}>Login</Link>
      </Col>
    );
    // (
    //   <Col span={1} style={{ textAlign: "right" }}>
    //     <Link to="/register">Register</Link>
    //   </Col>
    // );
  }

  private logout = (e: any) => {
    e.preventDefault();
    AuthUserObj.logout();
  }

  private login = (e: any) => {
    e.preventDefault();
    AuthUserObj.login();
  }

  private handleAuthentication = () => {
    AuthUserObj.handleAuthentication();
    return <div>Welcome!</div>;
  }

}

export default withRouter(MbLayout);
