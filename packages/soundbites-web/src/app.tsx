import { LocaleProvider } from "antd";
import "antd/dist/antd.css";
import enUS from "antd/lib/locale-provider/en_US";
import React from "react";
import * as ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "rxjs";
import "./app.less";
import MbLayout from "./layout";

ReactDOM.render((
  <BrowserRouter>
    <LocaleProvider locale={enUS}>
      <MbLayout  />
    </LocaleProvider>
  </BrowserRouter>
), document.getElementById("content"));
