import { doFetchPromise } from "../redux/helpers";
export const fetchUsers = () => doFetchPromise("http://localhost:3000/users", "GET")
