import { AuthUserObj } from "../auth";
import { doFetchPromise } from "../redux/helpers";
import { IYoutubeBite, IBite } from "../models";
export const fetchBites = (sort: string, page: number) => 
  doFetchPromise<IBite[]>(`http://localhost:3000/bites?order=${sort}&page=${page}`, "GET");
export const fetchUserBites = (userId?: string) => 
  doFetchPromise(`http://localhost:3000/users/${userId || encodeURIComponent(AuthUserObj.getProfile().sub)}/bites`, "GET")
export const fetchBite = (biteId: string) => 
  doFetchPromise<IBite>(`http://localhost:3000/bites/${biteId}`, "GET");
export const postClip = (youtubeClip: { videoRange: [number, number], youtubeCode: string }) => 
  doFetchPromise<IYoutubeBite>("http://localhost:3000/bites/youtube/clip", "POST", youtubeClip);
export const postBite = (bite: IBite) => 
  doFetchPromise<IBite>("http://localhost:3000/bites/", "POST", bite);
export const putBite = (biteId: string, bite: IBite) => 
  doFetchPromise<IBite>(`http://localhost:3000/bites/${biteId}`, "PUT", bite);