export interface IBite {
  _id: any;
  title?: string;
  audioFilename?: string;
  imageFilename?: string;
  tags?: string[];
  userId?: string;
  origin?: string;
  originReference?: string;
  originVideoRange?: [number, number];
  playCount?: number;
  createDate?: Date;
}

export default IBite;
