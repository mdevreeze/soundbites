export interface IYoutubeBite {
  youtubeCode: string;
  videoRange: [number, number];
  audioFilename?: string;
}

export default IYoutubeBite;
