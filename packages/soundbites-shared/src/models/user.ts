export interface IUser {
  email_verified: boolean;
  email: string;
  updated_at: string;
  name: string;
  picture: string;
  user_id: string;
  nickname: string;
  identities?: IdentitiesEntity[] | null;
  created_at: string;
  last_password_reset: string;
  last_ip: string;
  last_login: string;
  logins_count: number;
  app_metadata: any;
}

export interface IdentitiesEntity {
  user_id: string;
  provider: string;
  connection: string;
  isSocial: boolean;
}

export default IUser;
