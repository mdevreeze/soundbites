import * as React from "react";
import { IBite } from "../models/bite";
import { HttpStatus } from "../redux";
import { IYoutubeBite } from "../models";
import { postClip, fetchBite, postBite, putBite } from "../fetch/fetch-bites";

export const DraftContext = React.createContext({ draft: {} } as IDraftStore);

export interface IDraftStore {
  draft: IBite;
  processingStatus: HttpStatus;
  error: string;
  actions: {
    setYoutubeRange: (videoRange: [number, number]) => void;
    setYoutubeCode: (youtubeUrl: string) => void;
    onFileUploadSuccess: (filename: string, originalFilename: string) => void;
    loadExistingBite: (biteId: string) => void;
    doYoutubeClipping: () => void;
    resetDraft: () => void;
    saveDraft: (bite: IBite) => Promise<IBite>;
  }
}
export class DraftProvider extends React.Component<any, IDraftStore> {
  constructor(props: any) {
    super(props);
    this.state = {
      draft: {
        _id: undefined
      },
      error: undefined,
      processingStatus: HttpStatus.NotStarted,
      actions: {
        doYoutubeClipping: this.doYoutubeClipping,
        loadExistingBite: this.loadExistingBite,
        onFileUploadSuccess: this.onFileUploadSuccess,
        setYoutubeCode: this.setYoutubeCode,
        setYoutubeRange: this.setYoutubeRange,
        resetDraft: this.resetDraft,
        saveDraft: this.saveDraft
      }
    }
  }

  public render() {
    return <DraftContext.Provider value={this.state}>
      {this.props.children}
    </DraftContext.Provider>;
  }

  private getYoutubeCode = (youtubeUrl: string): string => {
    // tslint:disable-next-line:max-line-length
    const arr = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/.exec(youtubeUrl);
    if (arr && arr[5]) {
      return arr[5];
    }
    return null;
  }

  public resetDraft = () => {
    this.setState({
      draft: {
        _id: undefined,
      },
      error: undefined,
      processingStatus: HttpStatus.NotStarted,
      actions: this.state.actions
    });
  }

  public setYoutubeRange = (videoRange: [number, number]) => {
    const draft: IBite = {
      ...this.state.draft,
      originVideoRange: videoRange
    }
    this.setState({ ...this.state, draft })
  }

  public setYoutubeCode = (youtubeUrl: string) => {
    const youtubeCode = this.getYoutubeCode(youtubeUrl);
    if (!youtubeCode) {
      this.setState({ ...this.state, error: "Error setting Youtube code. Was the URL invalid?"})
    } else {
      const draft: IBite = {
        ...this.state.draft,
        _id: undefined, // a new youtube code is always a new bite
        origin: "youtube",
        originReference: youtubeCode
      }
      this.setState({ ...this.state, draft });
    }
  }

  public onFileUploadSuccess = (filename: string, originalFilename: string) => {
    let draft: IBite = {
      ...this.state.draft,
      _id: undefined,
      audioFilename: filename,
      origin: "upload",
      originReference: originalFilename,
      originVideoRange: undefined
    }
    this.setState({ ...this.state, draft });
  }

  public loadExistingBite = (biteId: string) => {
    fetchBite(biteId)
      .then((bite) => this.setState({
        ...this.state,
        draft: bite.data
      }))
      .catch((error) => {
        this.setState({
          ...this.state,
          error: error.toString()
        })
      });
  }

  public doYoutubeClipping = () => {
    const youtubeClip = {
      videoRange: this.state.draft.originVideoRange,
      youtubeCode: this.state.draft.originReference
    } as IYoutubeBite;
    return postClip(youtubeClip)
      .then((r) => {
        const draft: IBite = {
          ...this.state.draft,
          audioFilename: r.data.audioFilename,
          origin: "youtube",
          originReference: r.data.youtubeCode,
        };
        this.setState({
          ...this.state,
          draft
        })
      })
      .catch((error: Error) => {
        this.setState({
          ...this.state,
          error: error.toString()
        })
      });
  }

  public saveDraft: (newBite: IBite) => Promise<IBite> = (newBite: IBite) => {
    const bite = { ...this.state.draft, ...newBite };
    if (bite._id) {
      return putBite(bite._id, bite)
        .then(r => r.data)
        .catch(e => {
          this.setState({ ...this.state, error: e });
          return e;
        });
    } else {
      return postBite(bite)
        .then(r => r.data)
        .catch(e => {
          this.setState({ ...this.state, error: e });
          return e;
        });
    }
  }
}