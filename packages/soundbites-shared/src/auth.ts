import {Auth0UserProfile, WebAuth } from "auth0-js";
import history from "./history";

export class AuthUser {
  private userProfile: Auth0UserProfile;
  // tslint:disable-next-line:member-ordering
  public auth0 = new WebAuth({
    domain: 'soundbytes.eu.auth0.com',
    clientID: 'yVLlLUF7nRVFc7E5yWUDnE94F2OgZoVn',
    redirectUri: 'http://localhost:8080/callback',
    // audience: 'https://soundbytes.eu.auth0.com/userinfo',
    audience: 'http://localhost:3000/',
    responseType: 'token id_token',
    scope: 'openid profile read:bites edit:bites'
  });

  constructor() {
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
  }

  public logout() {
    // Clear access token and ID token from local storage
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    localStorage.removeItem("profile");
    // navigate to the home route
    this.userProfile = undefined;
    history.replace("/home");
  }

  public isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem("expires_at"));
    return new Date().getTime() < expiresAt;
  }

  public login() {
    this.auth0.authorize();
  }

  public handleAuthentication() {
    this.auth0.parseHash((err: any, authResult: any) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        this.retrieveProfile();
        history.replace("/home");
      } else if (err) {
        history.replace("/home");
      }
    });
  }
  public getAccessToken() {
    const accessToken = localStorage.getItem("access_token");
    if (!accessToken) {
      throw new Error("No access token found");
    }
    return accessToken;
  }

  public getProfile(): auth0.Auth0UserProfile {
    const storedProfile = localStorage.getItem("profile");
    if (this.userProfile || storedProfile) {
      return this.userProfile || JSON.parse(storedProfile);
    }
  }

  public retrieveProfile() {
    const accessToken = this.getAccessToken();
    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if (profile) {
        this.userProfile = profile;
        localStorage.setItem("profile", JSON.stringify(profile));
      }
    });
  }

  public userHasRoles(roles: string[]): boolean {
    if (!this.isAuthenticated()) {
      return false;
    }
    const profile = this.getProfile();
    if (!profile) {
      return false;
    }
    const profileRoles = (profile as any)["https://soundbites.eu/roles"];
    return profileRoles && roles.every((role) => profileRoles.indexOf(role) > -1);
  }

  private setSession(authResult: any) {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem("access_token", authResult.accessToken);
    localStorage.setItem("id_token", authResult.idToken);
    localStorage.setItem("expires_at", expiresAt);

    // navigate to the home route
    history.replace("/home");
  }

}

export const AuthUserObj = new AuthUser();
