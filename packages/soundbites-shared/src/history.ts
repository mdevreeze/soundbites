import { History } from "history";
import createHistory from "history/createBrowserHistory";

export const history: History = createHistory();
export default history;
