import { Observable } from "rxjs";

export function getHeaders(): Headers {
  const headers = new Headers();
  const accessToken = localStorage.getItem("access_token");
  headers.append("Content-Type", "application/json");
  if (accessToken) {
    headers.append("Authorization", "Bearer " + localStorage.getItem("access_token"));
  }
  return headers;
}

export function doFetchPromise<T>(url: string, method: string, body?: any): Promise<{ data: T, metadata: any }> {
  const parameters: { method: string, headers: Headers, body?: string } = {
    method,
    headers: getHeaders()
  };
  if (body) {
    parameters.body = JSON.stringify(body);
  }
  return fetch(url, parameters)
    .then((response) => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then((body) => {
      if (body.success !== undefined && body.success !== true) {
        throw Error(body.errors)
      }
      return body;
    })
}

export function doFetch(url: string, method: string, body?: any) {
  const parameters: { method: string, headers: Headers, body?: string } = {
    method,
    headers: getHeaders()
  };
  if (body) {
    parameters.body = JSON.stringify(body);
  }
  return Observable.from(fetch(url, parameters).then((response) => response.json()))
    .catch((err, selector) => {
      throw err;
    });
}
