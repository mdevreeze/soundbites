import { shallow } from "enzyme";
import "jest";
import * as React from "react";
import { IBite } from "../src/models";
import { DraftProvider } from "../src/providers/draft";

const bite: IBite = {
  _id: "",
   audioFilename: "somesound.mp3",
   createDate: new Date(),
   imageFilename: "someimage.jpg",
   origin: "youtube",
   originReference: "someref",
   playCount: 10,
   title: "Test bite",
   originVideoRange: [10, 15],
   tags: [],
   userId: "someuserid"
};
const TestCompontent = <DraftProvider >
  <p>Test</p>
  </DraftProvider>;
test("Test draft provider", () => {
  const wrapper = shallow(TestCompontent);
  const instance = (wrapper.instance() as DraftProvider);
  instance.setYoutubeCode(bite.originReference);
  const state = wrapper.state() as any;
  console.log(state);
  expect(state.originReference).toBe(bite.originReference)
});