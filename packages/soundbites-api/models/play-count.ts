import * as mongoose from "mongoose";
export interface IPlayCount {
    _id: any;
    ipHash: string;
    biteId: string;
    playDate: Date;
}

export interface IPlayCountModel extends IPlayCount, mongoose.Document { }

export const playCountSchema = new mongoose.Schema({
    ipHash: String,
    biteId: String,
    playDate: { type: Date, default: Date.now },
});

export const playCountModel = mongoose.model<IPlayCountModel>("playCounts", playCountSchema);
