import * as mongoose from "mongoose";
import { IBite } from "soundbites-shared";
import { Response } from "express";

export interface IBiteModel extends IBite, mongoose.Document { }

export const biteSchema = new mongoose.Schema({
    title: String,
    audioFilename: String,
    imageFilename: String,
    origin: String,
    originReference: String,
    tags: Array,
    userId: String,
    playCount: Number,
    createDate: { type: Date, default: Date.now },
});

export const checkBite = (res: Response, bite: IBite) => {
    if (!bite.title || bite.title.length < 0) {
        res.json({ message: "Name can't be empty", success: false });
        return false;
    }
    if (!bite.audioFilename || bite.audioFilename.length < 0) {
        res.json({ message: "Media file can't be null", success: false });
        return false;
    }
    return true;
}

export const biteModel = mongoose.model<IBiteModel>("bite", biteSchema);
