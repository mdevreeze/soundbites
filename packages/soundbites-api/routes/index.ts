import * as express from "express";
import {Router} from "express-serve-static-core";
const router: Router = express.Router();

/* GET home page. */
router.get("/", (req, res, next) => {
  res.render("index", { title: "Express" });
});

export default router;
