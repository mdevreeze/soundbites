// tslint:disable-next-line:no-var-requires
import * as express from "express";
import { IFileStorage } from "../lib/file-storage";
import { FileStorageMinio } from "../lib/file-storage-minio";

const fileStorage: IFileStorage = new FileStorageMinio();

export class MediaController {
    public router: express.Router;
    constructor() {
        this.router = express.Router();
        this.router.get("/covers/:imageId", (req, res, next) => {
            this.serveFile(req.params.imageId, "covers", "temp-covers", res);
        });

        this.router.get("/audio/:audioId", (req, res, next) => {
            this.serveFile(req.params.audioId, "audio", "temp-audio", res);
        });
    }
    private serveFile(fileId: string, collection: string, collectionFallback: string, res: express.Response) {
        fileStorage.fileExists(collection, fileId)
            .then(exist => {
                if (exist) {
                    fileStorage.getFile(collection, fileId)
                        .then(stream => stream.pipe(res))
                        .catch(err => res.status(500).send(err));
                } else {
                    fileStorage.getFile(collectionFallback, fileId)
                        .then(stream => stream.pipe(res))
                        .catch(err => res.status(404).send(err));
                }
            })
            .catch(err => {
                console.error(err);
                res.status(500).send(err)
            });
    }
}

export default MediaController;