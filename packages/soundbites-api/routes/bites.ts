import * as crypto from "crypto";
import * as express from "express";
import * as multer from "multer";
import * as fs from "fs";
import { IBite, IYoutubeBite } from "soundbites-shared";
import { IPlayCountModel, playCountModel } from "../models/play-count";
import { IDbRepository } from "../lib/db-repository";
import { IFileStorage } from "../lib/file-storage";
import { IYoutubeClipper } from "../lib/youtube-clipper";
import { returnError } from "../lib/helpers/errorHandling";
import { doPermissionCheck } from "../lib/helpers/permissionCheck";
import { checkBite } from "../models/bite";
import { audioCollection, coverCollection, tempAudioCollection, tempCoverCollection } from "../config/storage"
import * as jwt from "express-jwt";

import logger from "../config/logger";

export class BitesController {
  private dbRepo: IDbRepository;
  private fileStorage: IFileStorage;
  private youtubeClipper: IYoutubeClipper;

  public router: express.Router;

  constructor(jwtCheck: jwt.RequestHandler, dbRepo: IDbRepository, fileStorage: IFileStorage, youtubeClipper: IYoutubeClipper) {
    this.dbRepo = dbRepo;
    this.fileStorage = fileStorage;
    this.youtubeClipper = youtubeClipper;

    const multerObj = multer({ limits: { fieldSize: 3 * 1024 }, dest: "./uploads", }).single("file");

    this.router = express.Router();
    this.router.get("/", (req, res, next) => this.GetAllBites(req, res, next));
    this.router.post("/", jwtCheck, (req, res, next) => this.SaveBite(req, res, next));
    this.router.get("/:biteId", (req, res, next) => this.GetBiteById(req, res, next));
    this.router.put("/:biteId", jwtCheck, (req, res, next) => this.UpdateBite(req, res, next));
    this.router.delete("/:biteId", jwtCheck, (req, res, next) => this.DeleteBite(req, res, next));
    this.router.post("/upload", jwtCheck, multerObj, (req, res, next) => this.UploadFile(req, res, next));
    this.router.post("/cover/upload", jwtCheck, multerObj, (req, res, next) => this.UploadCover(req, res, next));
    this.router.post("/youtube/clip", jwtCheck, (req, res, next) => this.ClipYoutube(req, res, next));
    this.router.put("/:biteId/countPlay", (req, res, next) => this.CountPlay(req, res, next));
  }

  public async GetAllBites(req: express.Request, res: express.Response, next: express.NextFunction) {
    let order = "createDate";
    if (req.query.order === "popular") {
      order = "playCount";
    }
    if (req.query.order === "new") {
      order = "createDate";
    }
    let skip = 0;
    let page = 1;
    if (req.query.page) {
      page = parseInt(req.query.page, undefined);
      if (page) {
        skip = (page * 12) - 12;
      }
    }
    try {
      const count = await this.dbRepo.countBites();
      const bites = await this.dbRepo.getAllBites(order, skip, 12);
      res.json({ data: bites, metadata: { total: count, page, order: req.query.order } });
    } catch (err) {
      logger.error(err);
      returnError(res)(err);
    }
  }

  public async GetBiteById(req: express.Request, res: express.Response, next: express.NextFunction) {
    this.dbRepo.getBiteById(req.params.biteId)
      .then(result => res.json({ data: result, success: true }))
      .catch(returnError(res));
  }

  public async UploadFile(req: express.Request, res: express.Response, next: express.NextFunction) {
    this.fileStorage.saveFile("temp-audio", this.dbRepo.getNewObjectId(), fs.createReadStream(req.file.path))
      .then((id) => {
        fs.unlink(req.file.path, (err) => console.error(err));
        returnError(res);
      })
      .catch(err => {
        fs.unlink(req.file.path, (err) => console.error(err));
        returnError(res);
      });
  }

  public async UploadCover(req: express.Request, res: express.Response, next: express.NextFunction) {
    this.fileStorage.saveFile("temp-covers", this.dbRepo.getNewObjectId(), fs.createReadStream(req.file.path))
      .then((id) => {
        fs.unlink(req.file.path, (err) => console.error(err));
        returnError(res);
      })
      .catch(err => {
        fs.unlink(req.file.path, (err) => console.error(err));
        returnError(res);
      });
  }

  public async ClipYoutube(req: express.Request, res: express.Response, next: express.NextFunction) {
    logger.info(req.body);
    const youtubeByte = req.body as IYoutubeBite;
    const startTime = youtubeByte.videoRange[0];
    const endTime = youtubeByte.videoRange[1];
    const youtubeCode = youtubeByte.youtubeCode;

    if (isNaN(endTime) || isNaN(startTime) || !youtubeCode || youtubeCode.length > 11 || !youtubeCode.match(/[\w\-]+/)) {
      res.status(400).json({ success: false, errros: ["Invalid arguments"] })
      return;
    }

    this.youtubeClipper.clipYoutube(youtubeCode, startTime, endTime)
      .then((id) => {
        youtubeByte.audioFilename = id;
        res.json({ data: youtubeByte, success: true, });
      })
      .catch(returnError(res));
  }

  public async UpdateBite(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      const resByte = await this.dbRepo.getBiteById(req.params.biteId)
      const updatedBite = req.body as IBite;

      if (!checkBite(res, updatedBite) || !doPermissionCheck(req, res, updatedBite)) {
        return;
      }

      if (updatedBite.imageFilename && updatedBite.imageFilename.length > 0 && updatedBite.imageFilename !== resByte.imageFilename) {
        this.fileStorage.moveFile(tempCoverCollection, coverCollection, updatedBite.imageFilename)
      }

      if (updatedBite.audioFilename !== resByte.audioFilename) {
        this.fileStorage.moveFile(tempAudioCollection, audioCollection, updatedBite.audioFilename)
      }

      const newByte = {...resByte, ...updatedBite};
      res.json({ success: true, data: await this.dbRepo.saveBite(newByte) });
    } catch (e) {
      returnError(res);
    }
  }

  public async DeleteBite(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      const bite = await this.dbRepo.getBiteById(req.params.biteId)
      if (!doPermissionCheck(req, res, bite)) {
        return;
      }

      const deletedBite = await this.dbRepo.deleteBite(bite._id);
      this.DeleteBiteFiles(deletedBite);

      res.json({ success: true });
    } catch (err) {
      returnError(res)(err);
      logger.error(err);
    }
  }


  public async CountPlay(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      const bite = await this.dbRepo.getBiteById(req.params.biteId);
      let playCounts = await this.dbRepo.getPlayCount(bite._id);
      if (!playCounts || playCounts.length === 0) {
        const ipHash = crypto.createHash("sha1")
          .update(req.connection.remoteAddress)
          .digest("hex");
        const newPlayCount = new playCountModel({
          ipHash,
          playDate: new Date(),
          biteId: bite._id
        } as IPlayCountModel);
        this.dbRepo.savePlayCount(newPlayCount);
        bite.playCount = bite.playCount ? bite.playCount + 1 : 1;
        await this.dbRepo.saveBite(bite)
        res.json({ success: true });
      } else {
        // do't update play count when hash has already played this recently
        res.json({ success: true });
      }
    } catch (err) {
      returnError(res)(err);
    }
  }

  public async SaveBite(req: express.Request, res: express.Response, next: express.NextFunction) {
    const inputByte = req.body as IBite;
    inputByte._id = this.dbRepo.getNewObjectId();

    if (!checkBite(res, inputByte)) {
      return;
    }

    let p1, p2: Promise<boolean> = undefined;
    if (inputByte.imageFilename) {
      p1 = this.fileStorage.moveFile(tempCoverCollection, coverCollection, inputByte.imageFilename)
    }
    p2 = this.fileStorage.moveFile(tempAudioCollection, audioCollection, inputByte.audioFilename);

    try {
      await p1, p2;
      inputByte.userId = req.user.sub;
      res.json({ data: await this.dbRepo.saveBite(inputByte), success: true })
    } catch (err) {
      returnError(res);
    }
  }

  private async DeleteBiteFiles(deletedBite: IBite) {
    if (await this.fileStorage.fileExists("audio", deletedBite.audioFilename)) {
      this.fileStorage.deleteFile("audio", deletedBite.audioFilename);
    } else {
      logger.warn(`Trying to remove bite audio ${deletedBite.audioFilename} that didn't exist`, { bite: deletedBite._id, audio: deletedBite.audioFilename });
    }

    if (await this.fileStorage.fileExists("images", deletedBite.imageFilename)) {
      this.fileStorage.deleteFile("audio", deletedBite.imageFilename);
    } else {
      logger.warn(`Trying to remove bite audio ${deletedBite.imageFilename} that didn't exist`, { bite: deletedBite._id, image: deletedBite.imageFilename });
    }
  }

}

export default BitesController;