import * as express from "express";

import * as moment from "moment";
import * as request from "request";
import { jwtCheck } from "../jwt";
import { IDbRepository } from "../lib/db-repository";
import { returnError } from "../lib/helpers/errorHandling";

import logger from "../config/logger";

export class UserController {
  private dbRepo: IDbRepository;

  public router: express.Router;
  public manageAccessToken: string;
  public tokenExpire: Date;

  constructor(dbRepo: IDbRepository) {
    this.dbRepo = dbRepo;
    this.router = express.Router();
    this.router.get("/", jwtCheck, (req, res, next) => this.GetAllUsers(req, res, next));
    this.router.get("/:userId/bites", jwtCheck, (req, res, next) => this.GetUsersBites(req, res, next));
  }

  private getAuthManagementOptions() {
    return {
      method: "POST",
      url: "https://soundbytes.eu.auth0.com/oauth/token",
      headers: { "content-type": "application/json" },
      body:
        {
          grant_type: "client_credentials",
          client_id: "gykoGhE7sfQCOWdLha6u1ZDlxByjwvSg",
          client_secret: "suDd2rW4xvegr-aDARFmrdLaVIVUsb4h5lw7Yxy1WbpsBsrpUcZSCczAdCosvojU",
          audience: "https://soundbytes.eu.auth0.com/api/v2/"
        },
      json: true
    };
  }

  private getAuthManagementToken(cb: (error: any, accessToken: string) => any) {
    const options = this.getAuthManagementOptions();
    if (this.tokenExpire > new Date() && this.manageAccessToken) {
      cb(undefined, this.manageAccessToken);
      return;
    }
    request(options, (error, response, body) => {
      if (error) {
        // tslint:disable-next-line:no-console
        cb(error, undefined);
        return;
      }
      this.manageAccessToken = body.access_token;
      this.tokenExpire = moment().add(body.expires_in).toDate();
      cb(undefined, this.manageAccessToken);
    });
  }

  public GetAllUsers(req: express.Request, res: express.Response, next: express.NextFunction) {
    logger.info("Getting all users.");
    const roles = req.user["https://soundbytes.eu/roles"];
    if (roles && roles.indexOf("admin") <= -1) {
      res.json({ success: false, errros: ["Not permitted"] });
      logger.info(roles);
      return;
    }
    this.getAuthManagementToken((error, accessToken) => {
      if (error) {
        res.json({ success: false, errors: ["Error retrieving access key for Management API"] });
        return;
      }

      const auth: request.AuthOptions = {
        bearer: accessToken
      };
      request("https://soundbytes.eu.auth0.com/api/v2/users", { auth }, (mngError, response, body) => {
        if (error && body.success) {
          res.json({ success: false, errros: [mngError] });
        } else {
          res.json({ success: true, data: JSON.parse(body) });
        }
      });
    });
  }

  public async GetUsersBites(req: express.Request, res: express.Response, next: express.NextFunction) {
    const roles = req.user["https://soundbytes.eu/roles"];
    if (req.params.userId !== req.user.sub && (roles && roles.indexOf("admin") <= -1)) {
      res.json({ success: false, errors: ["Not permitted"] });
      return;
    }
    let order = "createDate";
    if (req.query.order === "popular") {
      order = "playCount";
    }
    if (req.query.order === "new") {
      order = "createDate";
    }
    let skip = 0;
    let page = 1;
    if (req.query.page) {
      page = parseInt(req.query.page, undefined);
      if (page) {
        skip = (page * 12) - 12;
      }
    }
    try {
      const count = await this.dbRepo.countBites();
      const bites = await this.dbRepo.getAllBites(order, skip, 12);
      res.json({ data: bites, metadata: { total: count, page, order: req.query.order } });
    } catch (err) {
      logger.error(err);
      returnError(res)(err);
    }
  }
}

export default UserController;
