import { IYoutubeClipper } from "./youtube-clipper";
import { IFileStorage } from "./file-storage";
import { IDbRepository } from "./db-repository";
import * as child from "child_process";
import * as fs from "fs";

export class YoutubeDlClipper implements IYoutubeClipper {
    private _fileStorage: IFileStorage;
    private _dbRepo: IDbRepository;

    constructor(fileStorage: IFileStorage, dbRepo: IDbRepository) {
        this._fileStorage = fileStorage;
        this._dbRepo = dbRepo;
    }

    clipYoutube(youtubeCode: string, startTime: number, endTime: number): Promise<string> {
        const totalTime = endTime - startTime;
        const promise = new Promise<string>((resolve, reject) => {
            const id = this._dbRepo.getNewObjectId();
            child.exec(`youtube-dl 'https://www.youtube.com/watch?v=${youtubeCode}' ` +
                `-o '/tmp/soundbites/${id}.%(ext)s' ` +
                `--audio-format mp3 ` +
                `-x ` +
                `--external-downloader ffmpeg ` +
                `--external-downloader-args '-ss ${startTime} -t ${totalTime}'`, (error: any, stdout: any, stderr: any) => {
                    if (error) {
                        reject(error);
                        return;
                    }
                    const filename = `/tmp/soundbites/${id}.mp3`;
                    const newFile = fs.createReadStream(filename)
                    return this._fileStorage.saveFile("temp-audio", id, newFile)
                      .then((result) => {
                        fs.unlink(filename, () => undefined);
                        newFile.destroy();
                        resolve(result);
                      })
                      .catch(reject)
                });
        });
        return promise;
    }

    normalize(clipId: string): Promise<void> {
      return new Promise((resolve, reject) => {});

    }
}