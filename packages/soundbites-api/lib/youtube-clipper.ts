export interface IYoutubeClipper {
    /**
     * Clip a Youtube video and return the new clip id
     * @param youtubeCode the youtube code of the video to clip
     * @param startTime start time of the clip
     * @param endTime end time of the clip
     */
    clipYoutube(youtubeCode: string, startTime: number, endTime: number): Promise<string>;

    /**
     * Apply audio loudness normalization on clip
     * @param clipId clip to normalize
     */
    normalize(clipId: string): Promise<void>;
}