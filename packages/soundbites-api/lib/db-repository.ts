import { IBite } from "soundbites-shared";
import { IPlayCount } from "../models/play-count";
/**
 * Db access interface
 */
export interface IDbRepository {
    /**
     * Gets all bites from db
     * @param sorting sort bites on 
     * @param skip skip first number of bites
     * @param limit limit number of bites result
     */
    getAllBites(sorting: string, skip: number, limit?: number): Promise<IBite[]>;

    /**
     * Gets all users bites from db
     * @param userId userId to get the bites for 
     * @param sorting sort bites on 
     * @param skip skip first number of bites
     * @param limit limit number of bites result
     */
    getUsersBites(userId: string, sorting: string, skip: number, limit?: number) :Promise<IBite[]>;

    /**
     * Get the total count of bites in the db 
     */
    countBites(): Promise<number>;

    /**
     * Get a bite by id
     * @param id bite identifier
     */
    getBiteById(id: string): Promise<IBite>;

    /**
     * Save a new or exisiting bite
     * @param bite bite to save
     */
    saveBite(bite: IBite): Promise<IBite>;

    /**
     * Delete a bite
     * @param bite Bite to delete
     */
    deleteBite(bite: IBite): Promise<IBite>;

    /**
     * Get the play count of a bite
     * @param biteId bite identifier to get the play count for
     */
    getPlayCount(biteId: string): Promise<IPlayCount[]>;

    /**
     * Save a new or existing play count
     * @param playCount play count to save
     */
    savePlayCount(playCount: IPlayCount): Promise<IPlayCount>;

    /**
     * Get an object id
     */
    getNewObjectId(): string;

}
