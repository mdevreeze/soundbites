import { IFileStorage } from "./file-storage";
import { minioClient } from "../config/object-storage";
import { Stream } from "stream";

export class FileStorageMinio implements IFileStorage {
    fileExists(collection: any, id: string): Promise<boolean> {
        return new Promise<boolean>(resolve => minioClient.getObject(collection, id)
            .then(result => result ? resolve(true) : resolve(false))
            .catch((err) => resolve(false)));
    }

    getFile(collection: string, id: string) {
        return minioClient.getObject(collection, id);
    }

    saveFile(collection: string, id: string, stream: Stream): Promise<string> {
        return minioClient.putObject(collection, id, stream).then(() => id);
    }

    deleteFile(collection: string, id: string): Promise<void> {
        return minioClient.removeObject(collection, id)
    }

    moveFile(fromCollection: string, toCollection: string, id: string): Promise<boolean> {
        return this.fileExists(fromCollection, id)
            .then(exists => {
                if (exists) {
                    this.getFile(fromCollection, id)
                        .then(stream => this.saveFile(toCollection, id, stream))
                        .catch(err => console.error(err))
                    this.deleteFile(fromCollection, id)
                } else {
                    console.error("File doesn't exist");
                }
                return exists;
            })
    }
}