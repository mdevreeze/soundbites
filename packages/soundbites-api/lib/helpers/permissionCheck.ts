import { Response, Request } from "express";
import { IBite } from "soundbites-shared";

export const doPermissionCheck = (req: Request, res: Response, bite: IBite,  ): boolean => {
    const roles = (req as any).user["https://soundbytes.eu/roles"];

    if (req.params.biteId && bite._id !== req.params.biteId) {
    res.json({ success: false, errors: ["Tried to update an other bite than was specified"] });
    return;
    }

    if (bite.userId !== (req as any).user.sub && (roles && roles.indexOf("admin") <= -1)) {
        res.json({ success: false, errors: ["No permission to update this bite"] });
        return false;
    }

    return true;
}