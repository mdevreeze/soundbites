import { Response } from "express";
export const returnError = (res: Response) => {
    return (err: any) => {
        // tslint:disable-next-line:no-console
        console.error(err)
        res.status(500).json({ success: false, errors: [err] });
    }
}