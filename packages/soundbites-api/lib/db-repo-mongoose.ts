import * as mongodb from "mongodb";
import * as mongoose from "mongoose";
import { IBite } from "soundbites-shared";
import { IDbRepository } from "./db-repository";
import { IBiteModel } from "../models/bite";
import { IPlayCount, playCountModel, IPlayCountModel } from "../models/play-count";

const ObjectID = mongodb.ObjectID;
export class RepoMongoose implements IDbRepository {
  private _biteModel:  mongoose.Model<IBiteModel>;
  public constructor(biteModel: mongoose.Model<IBiteModel>) {
    this._biteModel = biteModel;

  }
  public getNewObjectId(): string {
    return new ObjectID().toHexString();
  }

  public async getAllBites(sorting: string, skip: number, limit: number = 12): Promise<IBite[]> {
    return await this._biteModel.find(undefined, undefined, {
      sort: { [sorting]: -1 },
      skip,
      limit
    });
  }

  public async getUsersBites(userId: string, sorting: string, skip: number, limit: number = 12) {
    return this._biteModel.find({ userId }, undefined, {
      sort: { [sorting]: -1 },
      skip,
      limit
    });
  }

  public async countBites(): Promise<number> {
    return await this._biteModel.count(undefined).then((result) => result);
  }

  public async getBiteById(id: string): Promise<IBite> {
    return await this._biteModel.findById(id);
  }

  public async saveBite(bite: IBite): Promise<IBite> {
    if (bite instanceof this._biteModel && bite._id) {
      return await (bite as IBiteModel).save(undefined);
    } else {
      return await new this._biteModel(bite).save(undefined);
    }
  }

  public async deleteBite(bite: IBite): Promise<IBite> {
    if (bite instanceof this._biteModel) {
      return await (bite as IBiteModel).remove();
    } else {
      return await this._biteModel.findByIdAndRemove(bite._id);
    }
  }

  public async getPlayCount(biteId: string): Promise<IPlayCount[]> {
    const date = new Date();
    const lastHour = new Date();
    lastHour.setHours(date.getHours() - 1);
    return playCountModel.find({ biteId, playDate: { $gte: lastHour, $lt: date } });
  }

  public async savePlayCount(playCount: IPlayCount): Promise<IPlayCount> {
    if (playCount instanceof playCountModel && playCount._id) {
      return (playCount as IPlayCountModel).save();
    } else {
      const newPlayCount = new playCountModel(playCount);
      return newPlayCount.save();
    }
  }
}