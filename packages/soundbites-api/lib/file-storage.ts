import { Stream } from "stream";

/**
 * File storage interface
 */
export interface IFileStorage {
    /**
     * Does the file exist? 
     * @param collection collection to check if a file exists
     * @param id 
     */
    fileExists(collection: string, id: string): Promise<boolean>;

    /**
     * Get a file
     * @param collection collection to get the file from
     */
    getFile(collection: string, id: string): Promise<Stream>;

    /**
     * Moves a file
     * @param fromCollection from which collection to move the file
     * @param toCollection to which collection to move the file
     * @param id identifier of the file to move
     */
    moveFile(fromCollection: string, toCollection: string, id: string): Promise<boolean>;
        
    /**
     * Save a file
     * @param collection collection to save the file to
     * @param id file identifier
     */
    saveFile(collection: string, id: string, stream: Stream): Promise<string>;

    /**
     * Delete file
     * @param collection collection to delete the file from
     * @param id file identifier
     */
    deleteFile(collection: string, id: string): Promise<void>;
}