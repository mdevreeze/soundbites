import "mocha";
import * as sinon from "sinon";
import { assert } from "chai";
import * as supertest from "supertest";
import "sinon-mongoose";
import { IBite } from "soundbites-shared";
import { coverCollection, tempCoverCollection, tempAudioCollection, audioCollection } from "../../config/storage";
import * as request from "request";
import { MethodCallReturn } from "typemoq/MethodCallReturn";
import { IFileStorage } from "../../lib/file-storage";
import { createTestObjects, clientCredentialsOptions, getTestBites, mockFileStorageCall } from "../testHelpers";

const { app, biteModel, fileStorage } = createTestObjects();

describe("Bites crud route tests", () => {
  let token: string = undefined;
  let mock: sinon.SinonMock;

  before((done) => {
    mock = sinon.mock(biteModel)
    request(clientCredentialsOptions, function (error, response, body) {
      if (error) done(error);
      token = JSON.parse(body).access_token;
      done();
    });
  })

  describe("Get all bites", () => {
    const fakeBiteArr: any[] = [];
    for (let i = 0; i < 12; i++) {
      fakeBiteArr[i] = {};
    }
    before((done) => {
      mock.expects("count").resolves(fakeBiteArr.length);
      mock.expects("find").resolves(fakeBiteArr);
      done();
    })
    it(`should return ${fakeBiteArr.length} bites`, (done) => {
      supertest(app)
        .get("/bites")
        .set("Accept", "application/json")
        .expect(200)
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            assert.equal(res.body.data.length, fakeBiteArr.length, `Route should've returned ${fakeBiteArr.length} items`);
            mock.verify();
            done();
          }
        })
    })
    after(() => {
      mock.restore();
    })
  })

  describe("Save bite", () => {
    let moveImageCall: MethodCallReturn<IFileStorage, Promise<boolean>> = undefined;
    let moveAudioCall: MethodCallReturn<IFileStorage, Promise<boolean>> = undefined;
    let bite: IBite = undefined;
    let stub: sinon.SinonStub;
    before(() => {
      bite = getTestBites()[1];
      stub = sinon.stub(biteModel.prototype, 'save').resolves(bite);
      moveImageCall = mockFileStorageCall(fileStorage, tempCoverCollection, coverCollection, bite.imageFilename);
      moveAudioCall = mockFileStorageCall(fileStorage, tempAudioCollection, audioCollection, bite.audioFilename);
    })
    it("should successfully save a new bite", (done) => {

      supertest(app)
        .post("/bites")
        .auth(token, { type: "bearer" })
        .send(bite)
        .set({
          "Accept": "application/json",
        })
        .expect(200)
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            const newBite = res.body.data as IBite;
            assert.equal(moveImageCall.callCount, 1, "A call shoul've been made to move temporary image to final collection");
            assert.equal(moveAudioCall.callCount, 1, "A call shoul've been made to move temporary audio to final collection");
            assert.isNotEmpty(newBite._id, "Id shouldn't be empty");
            assert.equal(newBite.title, bite.title, "Title should've been the same")
            mock.verify();
            done();
          }
        })
    })
    after(() => {
      stub.restore();
    })
  })

  describe("Update bite", () => {
    let stub: sinon.SinonStub;
    let mock: sinon.SinonMock;
    let moveImageCall: MethodCallReturn<IFileStorage, Promise<boolean>> = undefined;
    let moveAudioCall: MethodCallReturn<IFileStorage, Promise<boolean>> = undefined;
    let oldBite: IBite = undefined;
    let newBite: IBite = undefined;

    before(() => {
      mock = sinon.mock(biteModel)
      oldBite = getTestBites()[1];
      newBite = {...oldBite, audioFilename: "2zzzzzzzzzea050ce661d13d.mp3"}

      mock.expects("findById").resolves(oldBite);
      stub = sinon.stub(biteModel.prototype, 'save').resolves(newBite);
      moveImageCall = mockFileStorageCall(fileStorage, tempCoverCollection, coverCollection, oldBite.imageFilename);
      moveAudioCall = mockFileStorageCall(fileStorage, tempAudioCollection, audioCollection, newBite.audioFilename);
    })

    it("should sucessfully update an existing bite", (done) => {
      supertest(app)
        .put(`/bites/${newBite._id}`)
        .auth(token, { type: "bearer" })
        .send(newBite)
        .set({
          "Accept": "application/json",
        })
        .expect(200)
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            const returnBite = res.body.data as IBite;
            assert.equal(moveImageCall.callCount, 0, "No call for image should've been made since it remainded the same");
            assert.equal(moveAudioCall.callCount, 1, "A call shoul've been made to move temporary audio to final collection");
            assert.isNotEmpty(returnBite._id, "Id shouldn't be empty");
            assert.equal(returnBite.title, oldBite.title, "Title should've been the same")
            assert.notEqual(returnBite.audioFilename, oldBite.audioFilename, "Audio filename shouldn't have been the same")
            mock.verify();
            done();
          }
        })
    })

    after(() => {
      mock.restore();
      stub.restore();
    })
  });
})
