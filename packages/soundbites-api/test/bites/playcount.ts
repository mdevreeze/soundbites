import "mocha";
import * as sinon from "sinon";
import { assert } from "chai";
import "sinon-mongoose";
import { IBite } from "soundbites-shared";
import { createTestObjects, clientCredentialsOptions, getTestBites, doSupertest } from "../testHelpers";
import { IPlayCount } from "../../models/play-count";
import * as request from "request";

const { app, biteModel, playCountModel } = createTestObjects();
describe("Updating playcount", () => {
  let token: string = undefined;
  let bite: IBite = undefined;
  let playCount: IPlayCount
  let currentPlaycount: number;
  let findBiteById: sinon.SinonExpectation;
  let findPlayCount: sinon.SinonExpectation;
  let saveStub: sinon.SinonStub;
  before(done => {
    request(clientCredentialsOptions, function (error, _response, body) {
      if (error) done(error);
      token = JSON.parse(body).access_token;
      done();
    });
  })

  before((done) => {
    bite = getTestBites()[0];
    playCount = {
      _id: "12345",
      biteId: bite._id,
      ipHash: "somehash",
      playDate: undefined // set in test
    };
    playCount.playDate = new Date();
    playCount.playDate.setHours(playCount.playDate.getHours() - 2);
    currentPlaycount = bite.playCount;
    done();
  })

  beforeEach(done => {
    saveStub = sinon.stub(biteModel.prototype, 'save').resolves({});
    findBiteById = sinon.mock(biteModel).expects("findById").once();
    findPlayCount = sinon.mock(playCountModel).expects("find").once();
    findBiteById.resolves(bite);
    done();
  })

  it("should not update playcount", done => {
    findPlayCount.resolves(playCount); // find playcount returns a recent playcount
    doSupertest(app, "put", `/bites/${bite._id}/countPlay`, token)
    .end((err) => {
        if (err) {
          done(err);
        } else {
          findBiteById.verify();
          findPlayCount.verify();
          assert.isFalse(saveStub.called, "Save stub on bitemodel shouln't been called since there was no playcount to update");
          assert.equal(bite.playCount, currentPlaycount, "count shouldn't have updated since last update was to recent");
          done();
        }
      })
  })

  it("should increase playcount", done => {
    findPlayCount.resolves(null); // find playcount returns no playcount because there were no recent ones
    doSupertest(app, "put", `/bites/${bite._id}/countPlay`, token)
      .end((err) => {
        if (err) {
          done(err);
        } else {
          findBiteById.verify();
          findPlayCount.verify();
          assert.isTrue(saveStub.called, "Save stub on bitemodel should've been called when increasing the playcount");
          done();
        }
      })
  })

  afterEach(() => {
    saveStub.restore();
    findPlayCount.restore();
    findBiteById.restore();
  })
})