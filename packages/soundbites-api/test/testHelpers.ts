import * as moq from "typemoq";
import * as fs from "fs";
import * as path from "path";
import * as express from "express";
import * as bodyParser from "body-parser";
import { IBite } from "soundbites-shared";
import { AUTH_CLIENT_ID, AUTH_CLIENT_SECRET, AUTH_AUDIENCE, AUTH_URL, jwtCheck } from "../config/auth";
import { IYoutubeClipper } from "../lib/youtube-clipper";
import { BitesController, } from "../routes/bites";
import { RepoMongoose } from "../lib/db-repo-mongoose";
import { biteModel } from "../models/bite";
import { IFileStorage } from "../lib/file-storage";
import { playCountModel } from "../models/play-count";
import * as supertest from "supertest";

export const createTestObjects = () => {
  const dbRepo = new RepoMongoose(biteModel);
  const fileStorage = moq.Mock.ofType<IFileStorage>();
  const youtubeClipper = moq.Mock.ofType<IYoutubeClipper>();
  const bitesController = new BitesController(jwtCheck, dbRepo, fileStorage.object, youtubeClipper.object)
  const app = express();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use("/bites", bitesController.router);
  return {
    app,
    fileStorage,
    youtubeClipper,
    bitesController,
    biteModel,
    playCountModel
  }
}

export const clientCredentialsOptions = {
  method: 'POST',
  url: AUTH_URL,
  headers: { 'content-type': 'application/json' },
  body: `{"client_id":"${AUTH_CLIENT_ID}","client_secret":"${AUTH_CLIENT_SECRET}","audience":"${AUTH_AUDIENCE}","grant_type":"client_credentials"}`
};

export const doSupertest = (app: express.Application, httpMethod: string, uri: string, token: string): supertest.Test => {
  const stest = (supertest(app) as any);
  return (stest[httpMethod](uri) as supertest.Test)
  .auth(token, { type: "bearer" })
  .set({
      "Accept": "application/json",
    })
  .expect(200)
}

export const getTestBites = () => JSON.parse(fs.readFileSync(path.join(__dirname, "assets", "bites.json"), "UTF-8")) as IBite[];

export const mockFileStorageCall = (fileStorage: moq.IMock<IFileStorage>, tempCollection: string, collection: string, filename: string) => {
  const call = fileStorage.setup(f => f.moveFile(moq.It.is((x => x == tempCollection)), moq.It.is(x => x == collection), moq.It.is(x => x == filename)));
  call.returns(() => new Promise<boolean>((res) => res(true)))
  return call;
}