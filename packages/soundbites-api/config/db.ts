import * as mongoose from "mongoose";

const host = process.env.DB_HOST || "localhost";
const port = process.env.DB_PORT || 27017;
const name = process.env.DB_NAME || "soundbites";

export const configMongoose = () => {
  mongoose.connect(`mongodb://${host}:${port}/${name}`);
  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error:"));
}

export default configMongoose;