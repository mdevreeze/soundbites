import * as jwt from "express-jwt";
import * as jwks from "jwks-rsa";

export const AUTH_CLIENT_SECRET = "";
export const AUTH_CLIENT_ID = "";
export const AUTH_URL = "";
export const AUTH_AUDIENCE = "";

export const jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: ""
  }),
  audience: AUTH_AUDIENCE,
  issuer: "",
  algorithms: ["RS256"]
});
