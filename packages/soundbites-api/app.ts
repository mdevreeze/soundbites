import * as bodyParser from "body-parser";
import * as express from "express";

import BiteController from "./routes/bites";
import UserController from "./routes/users";
import MediaController from "./routes/media";

import { IFileStorage } from "./lib/file-storage";
import { IYoutubeClipper } from "./lib/youtube-clipper";
import { RepoMongoose } from "./lib/db-repo-mongoose";
import { YoutubeDlClipper } from "./lib/youtubedl-clipper";
import { FileStorageMinio } from "./lib/file-storage-minio";
import { IDbRepository } from "./lib/db-repository";
import * as morgan from "morgan";

import { MorganStreamer } from "./config/logger";
import configMongoose from "./config/db";
import configCors from "./config/cors";

import { jwtCheck } from "./jwt";
import { biteModel } from "./models/bite";

export class Server {
  public app: express.Express;

  constructor(biteController: BiteController, userController: UserController, mediaController: MediaController) {
    this.app = express();
    configCors(this.app);
    configMongoose();

    this.app.use(morgan("combined", { stream: new MorganStreamer() }));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ type: "application/vnd.api+json" }));

    // this.app.use("/", ((req, res) => res.send("Soundbites API")));
    this.app.use("/bites", biteController.router);
    this.app.use("/users", userController.router);
    this.app.use("/media", mediaController.router);

    // catch 404 and forward to error handler
    this.app.use((req, res, next) => {
      const err = new Error("Not Found");
      (err as any).status = 404;
      next(err);
    });

    // test page for jwtTokens
    this.app.get("/authorized", jwtCheck, (req: express.Request, res: express.Response) => {
      res.send("Secured Resource");
    });

    // error handler
    this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.json({ success: false, errors: [err.status] });
    });
  }
}

/* Poor man's DI */
const dbRepo: IDbRepository = new RepoMongoose(biteModel);
const fileStorage: IFileStorage = new FileStorageMinio();
const youtubeClipper: IYoutubeClipper = new YoutubeDlClipper(fileStorage, dbRepo);

const mediaController = new MediaController();
const userController = new UserController(dbRepo);
const biteController = new BiteController(jwtCheck, dbRepo, fileStorage, youtubeClipper);
const server = new Server(biteController, userController, mediaController);
/* end DI */

module.exports = server.app;
